﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace WFCafci
{
    class ApiToken
    {
        public string accessToken { get; private set; }
        public string refreshToken { get; private set; }
        public long expiresIn { get; private set; }
        public string tokenType { get; private set; }
        private DateTime startDate;
        public AuthenticationHeaderValue authorizationHeader
        {
            get
            {
                if (tokenType == null || accessToken == null)
                {
                    return null;
                }
                return new AuthenticationHeaderValue(tokenType, accessToken);
            }
        }

        public ApiToken(string json)
        {
            var tokenJson = JsonConvert.DeserializeObject<dynamic>(json);
            if (tokenJson.error != null)
            {
                throw new Exception((string) tokenJson.error);
            } else {
                accessToken = tokenJson.access_token;
                refreshToken = tokenJson.refresh_token;
                expiresIn = tokenJson.expires_in;
                tokenType = tokenJson.token_type;
                startDate = DateTime.Now;
            }
        }

        public bool hasExpired()
        {
            double timePassed = DateTime.Now.Subtract(startDate).TotalSeconds;
            timePassed += 1;
            return (timePassed >= expiresIn);
        }
    }
}
