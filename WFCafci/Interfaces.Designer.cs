﻿namespace WFCafci
{
    partial class Interfaces
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Interfaces));
            this.currentFolder = new System.Windows.Forms.TextBox();
            this.getFolder = new System.Windows.Forms.Button();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSendSelection = new System.Windows.Forms.Button();
            this.selectFirma = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabs = new System.Windows.Forms.TabControl();
            this.unica = new System.Windows.Forms.TabPage();
            this.tablaUnica = new System.Windows.Forms.DataGridView();
            this.btnReload = new System.Windows.Forms.Button();
            this.timeFrom = new System.Windows.Forms.DateTimePicker();
            this.timeTo = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnError = new System.Windows.Forms.Button();
            this.separatorHeader = new System.Windows.Forms.Label();
            this.titleImage = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.sociedadGerenteTag = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCerrarSesion = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnDownloadRecibo = new System.Windows.Forms.Button();
            this.folderBrowserRecibo = new System.Windows.Forms.FolderBrowserDialog();
            this.btnEditar = new System.Windows.Forms.Button();
            this.versionName = new System.Windows.Forms.Label();
            this.statusTextBox = new System.Windows.Forms.Label();
            this.lblAtencion = new System.Windows.Forms.Label();
            this.apiTokenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabs.SuspendLayout();
            this.unica.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablaUnica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.titleImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.apiTokenBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // currentFolder
            // 
            this.currentFolder.Enabled = false;
            this.currentFolder.Location = new System.Drawing.Point(25, 118);
            this.currentFolder.Name = "currentFolder";
            this.currentFolder.Size = new System.Drawing.Size(839, 20);
            this.currentFolder.TabIndex = 0;
            // 
            // getFolder
            // 
            this.getFolder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(142)))), ((int)(((byte)(173)))));
            this.getFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.getFolder.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.getFolder.ForeColor = System.Drawing.Color.White;
            this.getFolder.Location = new System.Drawing.Point(882, 112);
            this.getFolder.Name = "getFolder";
            this.getFolder.Size = new System.Drawing.Size(80, 30);
            this.getFolder.TabIndex = 1;
            this.getFolder.Text = "Buscar...";
            this.getFolder.UseVisualStyleBackColor = false;
            this.getFolder.Click += new System.EventHandler(this.getFolderClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 141);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Interfaces";
            // 
            // btnSendSelection
            // 
            this.btnSendSelection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            this.btnSendSelection.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            this.btnSendSelection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendSelection.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnSendSelection.ForeColor = System.Drawing.Color.White;
            this.btnSendSelection.Location = new System.Drawing.Point(672, 670);
            this.btnSendSelection.Name = "btnSendSelection";
            this.btnSendSelection.Size = new System.Drawing.Size(60, 30);
            this.btnSendSelection.TabIndex = 6;
            this.btnSendSelection.Text = "Enviar";
            this.btnSendSelection.UseVisualStyleBackColor = false;
            this.btnSendSelection.Click += new System.EventHandler(this.btnSendSelection_Click);
            // 
            // selectFirma
            // 
            this.selectFirma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.selectFirma.FormattingEnabled = true;
            this.selectFirma.Location = new System.Drawing.Point(66, 643);
            this.selectFirma.Name = "selectFirma";
            this.selectFirma.Size = new System.Drawing.Size(902, 21);
            this.selectFirma.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 646);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Firma:";
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.unica);
            this.tabs.Location = new System.Drawing.Point(25, 157);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(944, 480);
            this.tabs.TabIndex = 9;
            this.tabs.SelectedIndexChanged += new System.EventHandler(this.onTabChange);
            this.tabs.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabs_Selecting);
            // 
            // unica
            // 
            this.unica.Controls.Add(this.tablaUnica);
            this.unica.Location = new System.Drawing.Point(4, 22);
            this.unica.Name = "unica";
            this.unica.Size = new System.Drawing.Size(936, 454);
            this.unica.TabIndex = 3;
            this.unica.Text = "Unica";
            this.unica.UseVisualStyleBackColor = true;
            // 
            // tablaUnica
            // 
            this.tablaUnica.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tablaUnica.BackgroundColor = System.Drawing.Color.White;
            this.tablaUnica.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tablaUnica.GridColor = System.Drawing.Color.White;
            this.tablaUnica.Location = new System.Drawing.Point(3, 3);
            this.tablaUnica.Name = "tablaUnica";
            this.tablaUnica.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tablaUnica.Size = new System.Drawing.Size(930, 451);
            this.tablaUnica.TabIndex = 1;
            this.tablaUnica.SelectionChanged += new System.EventHandler(this.fileSelectionChanged);
            // 
            // btnReload
            // 
            this.btnReload.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(142)))), ((int)(((byte)(173)))));
            this.btnReload.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(142)))), ((int)(((byte)(173)))));
            this.btnReload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReload.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnReload.ForeColor = System.Drawing.Color.White;
            this.btnReload.Location = new System.Drawing.Point(586, 670);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(80, 30);
            this.btnReload.TabIndex = 10;
            this.btnReload.Text = "Recuperar";
            this.btnReload.UseVisualStyleBackColor = false;
            this.btnReload.Click += new System.EventHandler(this.reloadFiles);
            // 
            // timeFrom
            // 
            this.timeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeFrom.Location = new System.Drawing.Point(727, 153);
            this.timeFrom.Name = "timeFrom";
            this.timeFrom.Size = new System.Drawing.Size(96, 20);
            this.timeFrom.TabIndex = 11;
            this.timeFrom.ValueChanged += new System.EventHandler(this.onTimeChange);
            // 
            // timeTo
            // 
            this.timeTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeTo.Location = new System.Drawing.Point(864, 153);
            this.timeTo.Name = "timeTo";
            this.timeTo.Size = new System.Drawing.Size(96, 20);
            this.timeTo.TabIndex = 12;
            this.timeTo.ValueChanged += new System.EventHandler(this.onTimeChange);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(686, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Desde:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(825, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Hasta:";
            // 
            // btnError
            // 
            this.btnError.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(203)))), ((int)(((byte)(139)))));
            this.btnError.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnError.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(203)))), ((int)(((byte)(139)))));
            this.btnError.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnError.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnError.ForeColor = System.Drawing.Color.White;
            this.btnError.Location = new System.Drawing.Point(738, 670);
            this.btnError.Name = "btnError";
            this.btnError.Size = new System.Drawing.Size(107, 30);
            this.btnError.TabIndex = 16;
            this.btnError.Text = "Detalle de envio";
            this.btnError.UseVisualStyleBackColor = false;
            this.btnError.Click += new System.EventHandler(this.btnError_Click);
            // 
            // separatorHeader
            // 
            this.separatorHeader.BackColor = System.Drawing.Color.Black;
            this.separatorHeader.Location = new System.Drawing.Point(-1, 75);
            this.separatorHeader.Name = "separatorHeader";
            this.separatorHeader.Size = new System.Drawing.Size(1024, 2);
            this.separatorHeader.TabIndex = 18;
            // 
            // titleImage
            // 
            this.titleImage.Image = ((System.Drawing.Image)(resources.GetObject("titleImage.Image")));
            this.titleImage.Location = new System.Drawing.Point(56, 12);
            this.titleImage.Name = "titleImage";
            this.titleImage.Size = new System.Drawing.Size(170, 60);
            this.titleImage.TabIndex = 17;
            this.titleImage.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(142)))), ((int)(((byte)(173)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(777, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 27);
            this.button1.TabIndex = 19;
            this.button1.Text = "Ir al CMS";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.goToCMS);
            // 
            // sociedadGerenteTag
            // 
            this.sociedadGerenteTag.AutoSize = true;
            this.sociedadGerenteTag.BackColor = System.Drawing.Color.Transparent;
            this.sociedadGerenteTag.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sociedadGerenteTag.Location = new System.Drawing.Point(174, 86);
            this.sociedadGerenteTag.MaximumSize = new System.Drawing.Size(600, 18);
            this.sociedadGerenteTag.Name = "sociedadGerenteTag";
            this.sociedadGerenteTag.Size = new System.Drawing.Size(76, 18);
            this.sociedadGerenteTag.TabIndex = 20;
            this.sociedadGerenteTag.Text = "cargando\r\n...";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 20);
            this.label5.TabIndex = 21;
            this.label5.Text = "Sociedad Gerente: ";
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.AutoSize = true;
            this.btnCerrarSesion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(142)))), ((int)(((byte)(173)))));
            this.btnCerrarSesion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarSesion.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnCerrarSesion.ForeColor = System.Drawing.Color.White;
            this.btnCerrarSesion.Location = new System.Drawing.Point(863, 29);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.Size = new System.Drawing.Size(98, 27);
            this.btnCerrarSesion.TabIndex = 1;
            this.btnCerrarSesion.Text = "Cerrar Sesión";
            this.btnCerrarSesion.UseVisualStyleBackColor = false;
            this.btnCerrarSesion.Click += new System.EventHandler(this.btnCerrarSesion_Click);
            // 
            // btnDownloadRecibo
            // 
            this.btnDownloadRecibo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            this.btnDownloadRecibo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            this.btnDownloadRecibo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDownloadRecibo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnDownloadRecibo.ForeColor = System.Drawing.Color.White;
            this.btnDownloadRecibo.Location = new System.Drawing.Point(851, 670);
            this.btnDownloadRecibo.Name = "btnDownloadRecibo";
            this.btnDownloadRecibo.Size = new System.Drawing.Size(117, 30);
            this.btnDownloadRecibo.TabIndex = 22;
            this.btnDownloadRecibo.Text = "Descargar recibo";
            this.btnDownloadRecibo.UseVisualStyleBackColor = false;
            this.btnDownloadRecibo.Click += new System.EventHandler(this.btnDownloadRecibo_Click);
            // 
            // folderBrowserRecibo
            // 
            this.folderBrowserRecibo.Description = "Selecciona la carpeta donde se descargarán los acuses de recibo.";
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(203)))), ((int)(((byte)(139)))));
            this.btnEditar.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnEditar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(203)))), ((int)(((byte)(139)))));
            this.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnEditar.ForeColor = System.Drawing.Color.White;
            this.btnEditar.Location = new System.Drawing.Point(517, 670);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(63, 30);
            this.btnEditar.TabIndex = 23;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = false;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // versionName
            // 
            this.versionName.AutoSize = true;
            this.versionName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionName.Location = new System.Drawing.Point(26, 679);
            this.versionName.Name = "versionName";
            this.versionName.Size = new System.Drawing.Size(48, 15);
            this.versionName.TabIndex = 24;
            this.versionName.Text = "Version";
            // 
            // statusTextBox
            // 
            this.statusTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statusTextBox.Location = new System.Drawing.Point(777, 84);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusTextBox.Size = new System.Drawing.Size(183, 20);
            this.statusTextBox.TabIndex = 25;
            this.statusTextBox.Text = "Listo";
            this.statusTextBox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.statusTextBox.Click += new System.EventHandler(this.label7_Click);
            // 
            // lblAtencion
            // 
            this.lblAtencion.AutoSize = true;
            this.lblAtencion.BackColor = System.Drawing.Color.Red;
            this.lblAtencion.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAtencion.ForeColor = System.Drawing.Color.White;
            this.lblAtencion.Location = new System.Drawing.Point(243, 12);
            this.lblAtencion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAtencion.Name = "lblAtencion";
            this.lblAtencion.Size = new System.Drawing.Size(515, 36);
            this.lblAtencion.TabIndex = 26;
            this.lblAtencion.Text = "ATENCION: ENTORNO DE PRUEBA";
            // 
            // apiTokenBindingSource
            // 
            this.apiTokenBindingSource.DataSource = typeof(WFCafci.ApiToken);
            // 
            // Interfaces
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1008, 690);
            this.Controls.Add(this.lblAtencion);
            this.Controls.Add(this.statusTextBox);
            this.Controls.Add(this.versionName);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnDownloadRecibo);
            this.Controls.Add(this.btnCerrarSesion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.sociedadGerenteTag);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.separatorHeader);
            this.Controls.Add(this.titleImage);
            this.Controls.Add(this.btnError);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.timeTo);
            this.Controls.Add(this.timeFrom);
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.tabs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.selectFirma);
            this.Controls.Add(this.btnSendSelection);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.getFolder);
            this.Controls.Add(this.currentFolder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Interfaces";
            this.Text = "Interfaces";
            this.tabs.ResumeLayout(false);
            this.unica.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tablaUnica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.titleImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.apiTokenBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox currentFolder;
        private System.Windows.Forms.Button getFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSendSelection;
        private System.Windows.Forms.ComboBox selectFirma;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.DateTimePicker timeFrom;
        private System.Windows.Forms.DateTimePicker timeTo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnError;
        private System.Windows.Forms.Label separatorHeader;
        private System.Windows.Forms.PictureBox titleImage;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label sociedadGerenteTag;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.BindingSource apiTokenBindingSource;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCerrarSesion;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnDownloadRecibo;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserRecibo;
        private System.Windows.Forms.TabPage unica;
        private System.Windows.Forms.DataGridView tablaUnica;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Label versionName;
        private System.Windows.Forms.Label statusTextBox;
        private System.Windows.Forms.Label lblAtencion;
    }
}

