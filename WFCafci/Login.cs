﻿using System;
using System.Windows.Forms;

namespace WFCafci
{
    public partial class Login : Form
    {
        private bool loginInProgress = false;

        public Login()
        {
            InitializeComponent();
            Helper.SetTitleVersion(this);
            versionName.Text += " " + Helper.VERSION;
            FormBorderStyle = FormBorderStyle.FixedSingle;
            /* Get current config */
            ApiHelper.onInit();
            AcceptButton = btnLogin;
            this.StartPosition = FormStartPosition.CenterScreen;

            //Label en rojo agregado para develop y staging.
            var modoComp = Helper.GetModoCompilacion();
            lblAtencion.Visible = modoComp != Helper.ModoCompilacion.Prod;
            if (modoComp != Helper.ModoCompilacion.Staging)
                lblAtencion.Text = modoComp.ToString().ToUpper();
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            if (this.loginInProgress)
            {
                return;
            } else
            {
                this.loginInProgress = true;
            }
            string error = await ApiHelper.login(user.Text, pass.Text);
            Console.WriteLine("Result: {0}", error);
            if (String.IsNullOrEmpty(error))
            {
                this.Hide();
                var form2 = new Interfaces(this);
                form2.Closed += (s, args) => this.Close();
                form2.Show();
            } else if (String.Equals(error, "pass-expired"))
            {
                MessageBox.Show(ApiHelper.translateError(error, true));
                ApiHelper.goToChangePass();
            } else
            {
                MessageBox.Show(ApiHelper.translateError(error, true));
            }
            this.loginInProgress = false;
        }
    }
}
