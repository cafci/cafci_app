﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFCafci
{
    public partial class ErrorsView : Form
    {
        private Interfaces ventanaPadre;
        private TabPage tabErrores;
        private TabPage tabWarnings;
        private TabPage tabErroresRecibo;

        private ActivosFaltantes ventanaActivosFaltantes;

        private List<Interfaz.ErrorData> selected;

        public ErrorsView(Interfaces ventanaPadre)
        {
            InitializeComponent();
            Helper.SetTitleVersion(this);
            this.ventanaPadre = ventanaPadre;
            this.tabErrores = tabs.TabPages["Errores"];
            this.tabWarnings = tabs.TabPages["Warnings"];
            this.tabErroresRecibo = tabs.TabPages["ErroresRecibo"];
            this.ventanaActivosFaltantes = new ActivosFaltantes(this);
            this.selected = new List<Interfaz.ErrorData>();
        }

        private void disableButton(Button btn)
        {
            btn.Enabled = false;
            btn.BackColor = System.Drawing.Color.LightGray;
            btn.Cursor = Cursors.No;
        }

        private void enableButton(Button btn)
        {
            btn.Enabled = true;
            btn.BackColor = btn.FlatAppearance.MouseOverBackColor;
            btn.Cursor = Cursors.Default;
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.ventanaPadre.Show();
        }

        internal void setTableDataSource(DataGridView table, object dataSource)
        {
            table.DataSource = null;
            table.DataSource = dataSource;
            table.ClearSelection();
            // Set default style
            table.AutoGenerateColumns = false;
            table.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            table.Columns[0].Width = 210;
            table.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            table.Columns[1].Width = 70;
        }

        internal void setInterfacesToShow(List<Interfaz> interfacesToShow)
        {
            List<Interfaz.ErrorData> errores = new List<Interfaz.ErrorData>();
            List<Interfaz.ErrorData> warnings = new List<Interfaz.ErrorData>();
            List<Interfaz.ErrorData> erroresRecibo = new List<Interfaz.ErrorData>();
            tabs.TabPages.Clear();
            bool foundErrores = false, foundWarnings = false, foundErroresRecibo = false;
            foreach (Interfaz interfaz in interfacesToShow)
            {
                if (interfaz.hasError())
                {
                    List<Interfaz.ErrorData> nuevosErrores = interfaz.getErrores();
                    errores.AddRange(nuevosErrores);
                    foundErrores = true;
                }
                
                if (interfaz.hasWarnings())
                {
                    warnings.AddRange(interfaz.getWarnings());
                    foundWarnings = true;
                }

                if (interfaz.hasErrorRecibo())
                {
                    erroresRecibo.Add(interfaz.getErrorRecibo());
                    foundErroresRecibo = true;
                }
            }
            
            if (foundErrores)
            {
                tabs.TabPages.Add(tabErrores);
                setTableDataSource(tablaErrores, errores);
            }

            if (foundWarnings)
            {
                tabs.TabPages.Add(tabWarnings);
                setTableDataSource(tablaWarnings, warnings);
            }

            if (foundErroresRecibo)
            {
                tabs.TabPages.Add(tabErroresRecibo);
                setTableDataSource(tablaErroresRecibo, erroresRecibo);
            }

            if (!foundErrores && !foundWarnings && !foundErroresRecibo)
            {
                throw new Exception("no-errors");
            }
        }

        private void fileSelectionChanged(object table, EventArgs e)
        {
            // Select current tab list
            // Get all the models of the selected rows
            selected.Clear();
            disableButton(detallesErrorBtn);
            
            foreach (DataGridViewRow row in ((DataGridView)table).SelectedRows)
            {
                var error = (Interfaz.ErrorData)row.DataBoundItem;
                selected.Add(error);
                if (error.codigo == "CT0034")
                {
                    Console.WriteLine("enabling button");
                    enableButton(detallesErrorBtn);
                }
            }

            if (selected.Count > 1)
            {
                Console.WriteLine("disabling button");
                disableButton(detallesErrorBtn);
            }
        }

        private void detallesErrorBtn_Click(object sender, EventArgs e)
        {

            if (selected == null || selected.Count <= 0)
            {
                MessageBox.Show("Primero seleccione un error");
                return;
            }
            try
            {
                this.ventanaActivosFaltantes.setErrorToShow(selected[0]);
                this.ventanaActivosFaltantes.ShowDialog();
            }
            catch (Exception excp)
            {
                Console.WriteLine("exception: {0}", excp.Message);
                MessageBox.Show("Hubo un error inesperado.");
            }


        }
    }
}
