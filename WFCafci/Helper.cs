﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace WFCafci
{
    class Helper
    {
        public const String VERSION = "5.1.0";
        //public const String VERSION = "4.2.0";

        public const String ORIGEN = "cliente-cafci-" + VERSION;

        //Estas son las tres posibilidades de compilacion del cliente
        public enum ModoCompilacion
        {
            Prod,
            Staging,
            Develop
        }

        //En este metodo es donde setea para que entorno va a ser compilado el cliente.
        public static ModoCompilacion GetModoCompilacion()
        {
            return ModoCompilacion.Prod;
        }

        public static void SetTitleVersion(Form window)
        {
            window.Text += " - SI CAFCI Cliente v" + VERSION;
        }

        /// <summary>
        /// Ajusta el listado del dropdown para ocupar todo el espacio posibles.
        /// </summary>
        public static void AdjustWidthComboBox_DropDown(ComboBox senderComboBox)
        {
            int width = senderComboBox.DropDownWidth;
            Graphics g = senderComboBox.CreateGraphics();
            Font font = senderComboBox.Font;
            int vertScrollBarWidth =
                (senderComboBox.Items.Count > senderComboBox.MaxDropDownItems)
                ? SystemInformation.VerticalScrollBarWidth : 0;

            int newWidth;
            foreach (string s in senderComboBox.Items)
            {
                newWidth = (int)g.MeasureString(s, font).Width
                    + vertScrollBarWidth;
                if (width < newWidth)
                {
                    width = newWidth;
                }
            }
            senderComboBox.DropDownWidth = width;
        }

        /// <summary>
        /// Obtiene todos los archivos para un directorio.
        /// </summary>
        public static List<String> walkPath(string path)
        {
            List<String> paths = new List<String>();
            if (File.Exists(path))
            {
                paths.Add(path);
            }
            else if (Directory.Exists(path))
            {
                IEnumerable<string> entries = Directory.EnumerateFileSystemEntries(path);
                foreach (string entry in entries)
                {
                    if (File.Exists(entry))
                    {
                        paths.Add(entry);
                    }
                }
            }
            return paths;
        }

        /// <summary>
        /// Obtiene todas las credenciales del sistema
        /// </summary>
        public static List<X509Certificate2> getCertificates()
        {
            List<X509Certificate2> certificates = new List<X509Certificate2>();
            X509Store my = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            my.Open(OpenFlags.ReadOnly);
            foreach (X509Certificate2 cert in my.Certificates)
            {
                certificates.Add(cert);
            }
            return certificates;
        }

        /// <summary>
        /// Obtiene una representacion entendible por un usuario de un certificado
        /// </summary>
        /// <param name="cert"></param>
        /// <returns></returns>
        public static string getCertificateName(X509Certificate2 cert)
        {
            string subjectName = null;
            string issuerName = null;
            string pattern = @"CN=([^,]+),";
            MatchCollection matches = Regex.Matches(cert.Subject, pattern);
            foreach (Match match in matches)
            {
                subjectName = match.Groups[1].Value;
            }
            matches = Regex.Matches(cert.Issuer, pattern);
            foreach (Match match in matches)
            {
                issuerName = match.Groups[1].Value;
            }
            if (subjectName == null)
            {
                return issuerName;
            }
            else if (issuerName == null)
            {
                return subjectName;
            }
            else
            {
                return subjectName + " ( " + issuerName + " )";
            }
        }

        /// <summary>
        /// Genera la firma del archivo
        /// </summary>
        public static async Task<String> signFile(String fileContents, RSACryptoServiceProvider csp)
        {
            return await Task.Run(() =>
            {
                byte[] fileData = Encoding.UTF8.GetBytes(fileContents);
                byte[] hash = MD5.Create().ComputeHash(fileData);
                byte[] signedHash = csp.SignHash(hash, CryptoConfig.MapNameToOID("MD5"));
                String signedString = Convert.ToBase64String(signedHash);
                //String hexHash = string.Concat(hash.Select(x => x.ToString("X2")));
                //Console.WriteLine("Hash: {0} Hex Hash: {1} Signed: {2}", Convert.ToBase64String(hash), hexHash, signedString);
                return signedString;
            });
        }
    }
}
