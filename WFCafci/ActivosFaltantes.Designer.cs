﻿namespace WFCafci
{
    partial class ActivosFaltantes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActivosFaltantes));
            this.separatorHeader = new System.Windows.Forms.Label();
            this.titleImage = new System.Windows.Forms.PictureBox();
            this.returnButton = new System.Windows.Forms.Button();
            this.tablaEstructuraPrincipal = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tablaInfo = new System.Windows.Forms.TableLayoutPanel();
            this.subclaseLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.claseLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ordenLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.interfazLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.entidadEmisoraLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.entidadOrganizadoraLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.nombreLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tipoTickerLabel = new System.Windows.Forms.Label();
            this.tickerLabel = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.mercadoLabel = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tipoCodigoMercadoLabel = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.codigoMercadoLabel = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.monedaLabel = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tipoCodigoMonedaLabel = new System.Windows.Forms.Label();
            this.codigoMonedaLabel = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.titleImage)).BeginInit();
            this.tablaEstructuraPrincipal.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tablaInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // separatorHeader
            // 
            this.separatorHeader.BackColor = System.Drawing.Color.Black;
            this.separatorHeader.Location = new System.Drawing.Point(4, 73);
            this.separatorHeader.Name = "separatorHeader";
            this.separatorHeader.Size = new System.Drawing.Size(1024, 2);
            this.separatorHeader.TabIndex = 31;
            // 
            // titleImage
            // 
            this.titleImage.Image = ((System.Drawing.Image)(resources.GetObject("titleImage.Image")));
            this.titleImage.Location = new System.Drawing.Point(61, 10);
            this.titleImage.Name = "titleImage";
            this.titleImage.Size = new System.Drawing.Size(170, 60);
            this.titleImage.TabIndex = 30;
            this.titleImage.TabStop = false;
            // 
            // returnButton
            // 
            this.returnButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(142)))), ((int)(((byte)(173)))));
            this.returnButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.returnButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.returnButton.ForeColor = System.Drawing.Color.White;
            this.returnButton.Location = new System.Drawing.Point(378, 30);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(98, 27);
            this.returnButton.TabIndex = 29;
            this.returnButton.Text = "Volver";
            this.returnButton.UseVisualStyleBackColor = false;
            this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // tablaEstructuraPrincipal
            // 
            this.tablaEstructuraPrincipal.ColumnCount = 1;
            this.tablaEstructuraPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tablaEstructuraPrincipal.Controls.Add(this.groupBox1, 0, 0);
            this.tablaEstructuraPrincipal.Location = new System.Drawing.Point(12, 78);
            this.tablaEstructuraPrincipal.Name = "tablaEstructuraPrincipal";
            this.tablaEstructuraPrincipal.RowCount = 1;
            this.tablaEstructuraPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.57471F));
            this.tablaEstructuraPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tablaEstructuraPrincipal.Size = new System.Drawing.Size(476, 229);
            this.tablaEstructuraPrincipal.TabIndex = 28;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tablaInfo);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(470, 223);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // tablaInfo
            // 
            this.tablaInfo.ColumnCount = 2;
            this.tablaInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.1513F));
            this.tablaInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.8487F));
            this.tablaInfo.Controls.Add(this.label26, 0, 14);
            this.tablaInfo.Controls.Add(this.codigoMonedaLabel, 0, 14);
            this.tablaInfo.Controls.Add(this.label24, 0, 13);
            this.tablaInfo.Controls.Add(this.tipoCodigoMonedaLabel, 0, 13);
            this.tablaInfo.Controls.Add(this.label22, 0, 12);
            this.tablaInfo.Controls.Add(this.monedaLabel, 0, 12);
            this.tablaInfo.Controls.Add(this.label20, 0, 11);
            this.tablaInfo.Controls.Add(this.codigoMercadoLabel, 0, 11);
            this.tablaInfo.Controls.Add(this.label18, 0, 10);
            this.tablaInfo.Controls.Add(this.tipoCodigoMercadoLabel, 0, 10);
            this.tablaInfo.Controls.Add(this.label16, 0, 9);
            this.tablaInfo.Controls.Add(this.mercadoLabel, 0, 9);
            this.tablaInfo.Controls.Add(this.label14, 0, 8);
            this.tablaInfo.Controls.Add(this.tickerLabel, 0, 8);
            this.tablaInfo.Controls.Add(this.tipoTickerLabel, 0, 7);
            this.tablaInfo.Controls.Add(this.label11, 0, 7);
            this.tablaInfo.Controls.Add(this.label10, 0, 6);
            this.tablaInfo.Controls.Add(this.nombreLabel, 0, 6);
            this.tablaInfo.Controls.Add(this.label7, 0, 5);
            this.tablaInfo.Controls.Add(this.entidadOrganizadoraLabel, 0, 5);
            this.tablaInfo.Controls.Add(this.label4, 0, 4);
            this.tablaInfo.Controls.Add(this.entidadEmisoraLabel, 0, 4);
            this.tablaInfo.Controls.Add(this.subclaseLabel, 1, 3);
            this.tablaInfo.Controls.Add(this.label9, 0, 3);
            this.tablaInfo.Controls.Add(this.claseLabel, 1, 2);
            this.tablaInfo.Controls.Add(this.label5, 0, 2);
            this.tablaInfo.Controls.Add(this.ordenLabel, 1, 1);
            this.tablaInfo.Controls.Add(this.label3, 0, 1);
            this.tablaInfo.Controls.Add(this.interfazLabel, 1, 0);
            this.tablaInfo.Controls.Add(this.label1, 0, 0);
            this.tablaInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablaInfo.Location = new System.Drawing.Point(3, 16);
            this.tablaInfo.Name = "tablaInfo";
            this.tablaInfo.RowCount = 15;
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tablaInfo.Size = new System.Drawing.Size(464, 204);
            this.tablaInfo.TabIndex = 4;
            // 
            // subclaseLabel
            // 
            this.subclaseLabel.AutoSize = true;
            this.subclaseLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.subclaseLabel.Location = new System.Drawing.Point(152, 39);
            this.subclaseLabel.Name = "subclaseLabel";
            this.subclaseLabel.Size = new System.Drawing.Size(309, 13);
            this.subclaseLabel.TabIndex = 9;
            this.subclaseLabel.Text = "Bonos Corporativos";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(3, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Subclase:";
            // 
            // claseLabel
            // 
            this.claseLabel.AutoSize = true;
            this.claseLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.claseLabel.Location = new System.Drawing.Point(152, 26);
            this.claseLabel.Name = "claseLabel";
            this.claseLabel.Size = new System.Drawing.Size(309, 13);
            this.claseLabel.TabIndex = 5;
            this.claseLabel.Text = "Titulos de Deuda";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Clase:";
            // 
            // ordenLabel
            // 
            this.ordenLabel.AutoSize = true;
            this.ordenLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ordenLabel.Location = new System.Drawing.Point(152, 13);
            this.ordenLabel.Name = "ordenLabel";
            this.ordenLabel.Size = new System.Drawing.Size(309, 13);
            this.ordenLabel.TabIndex = 3;
            this.ordenLabel.Text = "000001";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Orden";
            // 
            // interfazLabel
            // 
            this.interfazLabel.AutoSize = true;
            this.interfazLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.interfazLabel.Location = new System.Drawing.Point(152, 0);
            this.interfazLabel.Name = "interfazLabel";
            this.interfazLabel.Size = new System.Drawing.Size(309, 13);
            this.interfazLabel.TabIndex = 1;
            this.interfazLabel.Text = "SCC_IUnica_00010349_20170706_N002503";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Interfaz:";
            // 
            // entidadEmisoraLabel
            // 
            this.entidadEmisoraLabel.AutoSize = true;
            this.entidadEmisoraLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.entidadEmisoraLabel.Location = new System.Drawing.Point(152, 52);
            this.entidadEmisoraLabel.Name = "entidadEmisoraLabel";
            this.entidadEmisoraLabel.Size = new System.Drawing.Size(309, 13);
            this.entidadEmisoraLabel.TabIndex = 10;
            this.entidadEmisoraLabel.Text = "BR Malls";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Entidad Emisora:";
            // 
            // entidadOrganizadoraLabel
            // 
            this.entidadOrganizadoraLabel.AutoSize = true;
            this.entidadOrganizadoraLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.entidadOrganizadoraLabel.Location = new System.Drawing.Point(152, 65);
            this.entidadOrganizadoraLabel.Name = "entidadOrganizadoraLabel";
            this.entidadOrganizadoraLabel.Size = new System.Drawing.Size(309, 13);
            this.entidadOrganizadoraLabel.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(143, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Entidad Organizadora:";
            // 
            // nombreLabel
            // 
            this.nombreLabel.AutoSize = true;
            this.nombreLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nombreLabel.Location = new System.Drawing.Point(152, 78);
            this.nombreLabel.Name = "nombreLabel";
            this.nombreLabel.Size = new System.Drawing.Size(309, 13);
            this.nombreLabel.TabIndex = 14;
            this.nombreLabel.Text = "BC BR Malls";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(3, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(143, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Nombre:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(3, 91);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(143, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Tipo de Ticker / Alias:";
            // 
            // tipoTickerLabel
            // 
            this.tipoTickerLabel.AutoSize = true;
            this.tipoTickerLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tipoTickerLabel.Location = new System.Drawing.Point(152, 91);
            this.tipoTickerLabel.Name = "tipoTickerLabel";
            this.tipoTickerLabel.Size = new System.Drawing.Size(309, 13);
            this.tipoTickerLabel.TabIndex = 17;
            this.tipoTickerLabel.Text = "I";
            // 
            // tickerLabel
            // 
            this.tickerLabel.AutoSize = true;
            this.tickerLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tickerLabel.Location = new System.Drawing.Point(152, 104);
            this.tickerLabel.Name = "tickerLabel";
            this.tickerLabel.Size = new System.Drawing.Size(309, 13);
            this.tickerLabel.TabIndex = 18;
            this.tickerLabel.Text = "UPSG1593PAB43";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Location = new System.Drawing.Point(3, 104);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(143, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "Ticker / Alias:";
            // 
            // mercadoLabel
            // 
            this.mercadoLabel.AutoSize = true;
            this.mercadoLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mercadoLabel.Location = new System.Drawing.Point(152, 117);
            this.mercadoLabel.Name = "mercadoLabel";
            this.mercadoLabel.Size = new System.Drawing.Size(309, 13);
            this.mercadoLabel.TabIndex = 20;
            this.mercadoLabel.Text = "OTC";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Location = new System.Drawing.Point(3, 117);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(143, 13);
            this.label16.TabIndex = 21;
            this.label16.Text = "Mercado:";
            // 
            // tipoCodigoMercadoLabel
            // 
            this.tipoCodigoMercadoLabel.AutoSize = true;
            this.tipoCodigoMercadoLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tipoCodigoMercadoLabel.Location = new System.Drawing.Point(152, 130);
            this.tipoCodigoMercadoLabel.Name = "tipoCodigoMercadoLabel";
            this.tipoCodigoMercadoLabel.Size = new System.Drawing.Size(309, 13);
            this.tipoCodigoMercadoLabel.TabIndex = 22;
            this.tipoCodigoMercadoLabel.Text = "C";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Location = new System.Drawing.Point(3, 130);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(143, 13);
            this.label18.TabIndex = 23;
            this.label18.Text = "Tipo de Codigo Mercado:";
            // 
            // codigoMercadoLabel
            // 
            this.codigoMercadoLabel.AutoSize = true;
            this.codigoMercadoLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.codigoMercadoLabel.Location = new System.Drawing.Point(152, 143);
            this.codigoMercadoLabel.Name = "codigoMercadoLabel";
            this.codigoMercadoLabel.Size = new System.Drawing.Size(309, 13);
            this.codigoMercadoLabel.TabIndex = 24;
            this.codigoMercadoLabel.Text = "OT";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Location = new System.Drawing.Point(3, 143);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(143, 13);
            this.label20.TabIndex = 25;
            this.label20.Text = "Codigo Mercado:";
            // 
            // monedaLabel
            // 
            this.monedaLabel.AutoSize = true;
            this.monedaLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.monedaLabel.Location = new System.Drawing.Point(152, 156);
            this.monedaLabel.Name = "monedaLabel";
            this.monedaLabel.Size = new System.Drawing.Size(309, 13);
            this.monedaLabel.TabIndex = 26;
            this.monedaLabel.Text = "Dolar Estadounidense";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Location = new System.Drawing.Point(3, 156);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(143, 13);
            this.label22.TabIndex = 27;
            this.label22.Text = "Moneda:";
            // 
            // tipoCodigoMonedaLabel
            // 
            this.tipoCodigoMonedaLabel.AutoSize = true;
            this.tipoCodigoMonedaLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tipoCodigoMonedaLabel.Location = new System.Drawing.Point(152, 169);
            this.tipoCodigoMonedaLabel.Name = "tipoCodigoMonedaLabel";
            this.tipoCodigoMonedaLabel.Size = new System.Drawing.Size(309, 13);
            this.tipoCodigoMonedaLabel.TabIndex = 28;
            this.tipoCodigoMonedaLabel.Text = "C";
            // 
            // codigoMonedaLabel
            // 
            this.codigoMonedaLabel.AutoSize = true;
            this.codigoMonedaLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.codigoMonedaLabel.Location = new System.Drawing.Point(152, 182);
            this.codigoMonedaLabel.Name = "codigoMonedaLabel";
            this.codigoMonedaLabel.Size = new System.Drawing.Size(309, 22);
            this.codigoMonedaLabel.TabIndex = 30;
            this.codigoMonedaLabel.Text = "USD";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Location = new System.Drawing.Point(3, 169);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(143, 13);
            this.label24.TabIndex = 29;
            this.label24.Text = "Tipo de Codigo Moneda:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Location = new System.Drawing.Point(3, 182);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(143, 22);
            this.label26.TabIndex = 31;
            this.label26.Text = "Codigo Moneda:";
            // 
            // ActivosFaltantes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(500, 319);
            this.Controls.Add(this.separatorHeader);
            this.Controls.Add(this.titleImage);
            this.Controls.Add(this.returnButton);
            this.Controls.Add(this.tablaEstructuraPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ActivosFaltantes";
            this.Text = "Datos Activo Faltante";
            ((System.ComponentModel.ISupportInitialize)(this.titleImage)).EndInit();
            this.tablaEstructuraPrincipal.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tablaInfo.ResumeLayout(false);
            this.tablaInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label separatorHeader;
        private System.Windows.Forms.PictureBox titleImage;
        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.TableLayoutPanel tablaEstructuraPrincipal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tablaInfo;
        private System.Windows.Forms.Label subclaseLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label claseLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label ordenLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label interfazLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label codigoMercadoLabel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label tipoCodigoMercadoLabel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label mercadoLabel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label tickerLabel;
        private System.Windows.Forms.Label tipoTickerLabel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label nombreLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label entidadOrganizadoraLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label entidadEmisoraLabel;
        private System.Windows.Forms.Label codigoMonedaLabel;
        private System.Windows.Forms.Label tipoCodigoMonedaLabel;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label monedaLabel;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
    }
}