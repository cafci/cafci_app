﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFCafci
{
    public partial class EditarInterfaces : Form
    {
        private Interfaces ventanaPadre;
        private Interfaz interfaz;
        private InterfazXmlHelper.InterfazXmlData interfazData;

        public EditarInterfaces(Interfaces ventanaPadre)
        {
            //durationTextBox.DataBindings.Clear();
            InitializeComponent();
            Helper.SetTitleVersion(this);
            this.ventanaPadre = ventanaPadre;

            durationTextBox.Validating += DurationTextBox_Validating;
        }

        private void DurationTextBox_Validating(object sender, CancelEventArgs e)
        {
            float aux;
            bool isInt = float.TryParse(durationTextBox.Text, out aux);
            if (!isInt)
            {
                durationErrLabel.Text = "Valor inválido, debe ser un número.";
                durationErrLabel.Show();
                e.Cancel = true;
            } else
            {
                durationErrLabel.Hide();
            }
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.ventanaPadre.Show();
        }

        internal void setInterfacesToShow(List<Interfaz> interfaces)
        {
            interfaz = interfaces[0];
            interfazData = interfaz.parsedData;

            interfazLabel.Text = interfaz.generateFileName();
            sociedadLabel.Text = interfaz.sociedad;
            fondoLabel.Text = interfaz.fondo;
            fechaLabel.Text = interfaz.date.ToShortDateString();

            if (interfazData.carteras != null)
            {
                durationTextBox.Enabled = true;
                durationTextBox.Text = interfazData.carteras.duration == null ? "" : interfazData.carteras.duration;
                durationErrLabel.Hide();
            } else
            {
                durationTextBox.Enabled = false;
                durationTextBox.Text = "";
                durationErrLabel.Text = "No hay datos semanales para editar.";
                durationErrLabel.Show();
            }
        }

        private void guardarButton_Click(object sender, EventArgs e)
        {
            if (interfazData.carteras != null)
            {
                interfazData.carteras.duration = durationTextBox.Text;
            }

            InterfazXmlHelper.writeInterfaz(interfaz.path, interfazData);

            this.Hide();
            this.ventanaPadre.Show();
        }
    }
}
