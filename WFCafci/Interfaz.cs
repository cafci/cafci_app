﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;

namespace WFCafci
{
    class Interfaz
    {
        [Browsable(false)]
        public string tipo { get; private set; }
        [Browsable(false)]
        public string path { get; private set; }
        [Browsable(false)]
        public DateTime date { get; private set; }
        [DisplayName("Archivo")]
        public string name { get; private set; }
        [DisplayName("Fecha")]
        public string fecha { get; private set; }
        [DisplayName("Sociedad")]
        [Browsable(false)]
        public string sociedad { get; private set; }
        [DisplayName("Fondo")]
        public string fondo { get; private set; }
        [DisplayName("Estado")]
        public string status { get; private set; }
        [DisplayName("Estado Recibo")]
        public string statusRecibo { get; private set; }
        [DisplayName("Referencia")]
        public string referencia { get; private set; }
        // [Browsable(false)]
        // public ErrorData error { get; set; }
        [Browsable(false)]
        public ErrorData errorRecibo { get; set; }
        [Browsable(false)]
        public string relativePath { get; private set; }
        [Browsable(false)]
        public bool enviando { get; private set; }
        [Browsable(false)]
        public bool hayError { get; private set; }
        [Browsable(false)]
        public bool enviado { get; private set; }


        dynamic fondoData;
        public InterfazXmlHelper.InterfazXmlData parsedData;
        public Interfaces ventanaInterfaces;
        private string sociedadNumber;
        private string fondoNumber;
        private string fechaNumber;
        private List<ErrorData> warnings;
        private List<ErrorData> errors;

        public class ErrorData
        {
            [Browsable(false)]
            public string path { get; set; }
            [DisplayName("Interfaz")]
            public string interfazFilename { get; set; }
            [DisplayName("Código")]
            public string codigo { get; set; }
            [Browsable(false)]
            public string extraData { get; set; }
            [DisplayName("Descripción")]
            public string descripcion { get; set; }

            public ErrorData(string path, string interfazFilename, string codigo, string extraData, string descripcion)
            {
                this.path = path;
                this.interfazFilename = interfazFilename;
                this.codigo = codigo;
                this.extraData = extraData;
                this.descripcion = descripcion;
            }
        }

        public Interfaz(Interfaces ventanaInterfaces, string fullPath, Dictionary<string, string> sociedades, Dictionary<string,dynamic> fondos){
            this.ventanaInterfaces = ventanaInterfaces;
            tipo = "Unica";
            path = fullPath;
            name = path.Substring(path.LastIndexOf("\\") + 1);
            relativePath = path.Substring(0, path.LastIndexOf("\\") + 1);
            status = "Recuperado";
            statusRecibo = "";
            enviando = false;
            hayError = false;
            enviado = false;
            errors = new List<ErrorData>();
            generateDataFromName();
            if (sociedades != null)
            {
                string sociedadName;
                if (sociedades.TryGetValue(sociedad, out sociedadName))
                {
                    sociedad = sociedadName;
                }
            }
            if(fondos != null){
                if (fondos.TryGetValue(fondo, out fondoData))
                {
                    fondo = fondoData.nombre.ToString();
                } else
                {
                    // Error: No codigo
                    onError("GT0002|"+ fondo);
                }
            }

            parsedData = InterfazXmlHelper.readInterfaz(path);
        }

        private DateTime getDateTime()
        {
            if (fecha == null)
            {
                return DateTime.Now;
            }
            var format = "yyyyMMdd";
            return DateTime.ParseExact(fecha, format,
                                       System.Globalization.CultureInfo.InvariantCulture);
        }

        public string generateFileName()
        {
            // I_CAFCI_GGGGFFFF_aaaammdd.xml
            string fileName = "I_CAFCI" + "_" + sociedadNumber + fondoNumber + "_" + fechaNumber;
            if (referencia != null)
            {
                fileName += "_" + referencia;
            }
            fileName += ".xml";
            return fileName;
        }

        private void generateDataFromName()
        {
            // I_CAFCI_00290111_20140926
            string pattern = @"^I_CAFCI_(\d{4})(\d{4})?_(\d+)(_N?\d+)?\.xml$";
            MatchCollection matches = Regex.Matches(name, pattern);
            foreach (Match match in matches)
            {
                sociedad = match.Groups[1].Value;
                sociedadNumber = sociedad;
                fondo = match.Groups[2].Value;
                fondoNumber = fondo;
                fecha = match.Groups[3].Value;
                fechaNumber = fecha;
                date = getDateTime();
                string refString = match.Groups[4].Value;
                if(!String.IsNullOrEmpty(refString))
                {
                    referencia = refString.Replace("_", "");
                    status = "Recuperado.";
                } else
                {
                    referencia = null;
                }
                fecha = date.ToString("dd/MM/yyyy");
            }
        }

        public bool isSameSociedad(string sociedadName)
        {
            return (sociedadName == this.sociedad);
        }

        public void completingFile()
        {
            status = "Completando...";
            enviando = true;
            hayError = false;
            enviado = false;
            this.ventanaInterfaces.updateViewTabla(this.tipo);
        }

        public void sendingFile()
        {
            status = "Enviando...";
            enviando = true;
            hayError = false;
            enviado = false;
            this.ventanaInterfaces.updateViewTabla(this.tipo);
        }

        public void sendedFile(string newReferencia)
        {
            enviando = false;
            hayError = false;
            enviado = true;

            referencia = newReferencia;
            if (warnings != null)
            {
                status = "Enviado con aviso.";
            }
            else
            {
                status = "Enviado.";
            }
            this.ventanaInterfaces.updateViewTabla(this.tipo);
        }

        public void gettingRecibo()
        {
            statusRecibo = "Obteniendo Recibo...";
            this.ventanaInterfaces.updateViewTabla(this.tipo);
        }

        public void gotRecibo()
        {
            statusRecibo = "Recibo descargado";
            this.ventanaInterfaces.updateViewTabla(this.tipo);
        }

        public void changePath(string newPath)
        {
            path = newPath;
            name = path.Substring(path.LastIndexOf("\\") + 1);
            relativePath = path.Substring(0, path.LastIndexOf("\\") + 1);
        }

        public void onError(string err)
        {
            if (err == "worker-died")
            {
                err = "invalid-interfaz";
            }
            enviando = false;
            hayError = true;
            enviado = true;
            if (errors == null)
            {
                errors = new List<ErrorData>();
            } 
            string extraData = null;
            string originalErr = err;
            if (err.Contains("|"))
            {
                int separatorIndex = err.IndexOf("|");
                extraData = err.Substring(separatorIndex + 1);
                err = err.Substring(0, separatorIndex);
            }
            

            if (err == "GT0012")
            {
                var jsonExtraData = JsonConvert.DeserializeObject<dynamic>(extraData);
                foreach (var error in jsonExtraData.errors)
                {
                    string dummyData = error.field + "-" + error.element;
                    string dummyErr = err + "|" + dummyData;
                    string errorDescription = ApiHelper.translateError(dummyErr, false, this.generateFileName());
                    errors.Add(new ErrorData(this.path, this.generateFileName(), err, dummyData, errorDescription));
                }
            }
            else
            {
                string errorDescription = ApiHelper.translateError(originalErr, false, this.generateFileName());
                errors.Add(new ErrorData(this.path, this.generateFileName(), err, extraData, errorDescription));
            }
            status = "Error";
            this.ventanaInterfaces.updateViewTabla(this.tipo);
        }

        public void onErrorRecibo(string err)
        {
            if (err == "worker-died") {
                err = "invalid-interfaz";
            }
            hayError = true;
            string errorDescription = ApiHelper.translateError(err, false, this.generateFileName());
            string extraData = null;
            if (err.Contains("|"))
            {
                int separatorIndex = err.IndexOf("|");
                extraData = err.Substring(separatorIndex + 1);
                err = err.Substring(0, separatorIndex);
            }
            errorRecibo = new ErrorData(this.path, this.generateFileName(), err, extraData, errorDescription);
            statusRecibo = "Error";
            this.ventanaInterfaces.updateViewTabla(this.tipo);
        }
        
        public void onWarning(dynamic warnings)
        {
            if (warnings == null)
            {
                return;
            }
            this.warnings = new List<ErrorData>();

            List<dynamic> tempArray = new List<dynamic>();
            foreach (dynamic warning in warnings)
            {
                string extraData = "";
                try
                {
                    extraData = ((int)warning.data).ToString("D6");
                }
                catch (Exception e)
                {
                    extraData = "";
                }
                warning.sortField = warning.warn + extraData;
                tempArray.Add(warning);
            }

            Comparison<dynamic> comparison = (warn1, warn2) => String.Compare((string)warn1.sortField, (string)warn2.sortField);
            tempArray.Sort(comparison);
            warnings = tempArray;
                        
            foreach (dynamic warning in warnings)
            {
                this.warnings.Add(new ErrorData(this.path, this.generateFileName(), (string)(warning.warn), null, ApiHelper.translateWarning(warning, this.generateFileName())));
            }
            this.ventanaInterfaces.updateViewTabla(this.tipo);
        }

        public bool hasError()
        {
            return status == "Error";
        }

        public List<ErrorData> getErrores()
        {
            return errors;
        }

        public void resetErrores()
        {
            errors = new List<ErrorData>();
        }

        public bool hasErrorRecibo()
        {
            return statusRecibo == "Error";
        }

        public ErrorData getErrorRecibo()
        {
            return errorRecibo;
        }

        public bool hasWarnings()
        {
            return warnings != null;
        }

        public List<ErrorData> getWarnings()
        {
            return warnings;
        }

        public bool allowEditing()
        {
            if (fondoData == null)
            {
                return false;
            }
            // Permitir editar interfaces que tengan carteras y cuyo tipo de renta sea "Renta Fija" (id: 3) o "Mercado de Dinero" (id: 4)
            return (parsedData.carteras != null && (fondoData.tipoRentaId == 3 || fondoData.tipoRentaId == 4)); 
        }


        // public string getErrorMessage()
        // {
        //     string errorMessage = error;
        //     if (warnings != null)
        //     {
        //         errorMessage = "";
        //         foreach (string warning in warnings)
        //         {
        //             if (errorMessage == "")
        //             {
        //                 errorMessage = warning;
        //             } else
        //             {
        //                 errorMessage += "\n" + warning;
        //             }
        //         }
        //     }
        //     return errorMessage;
        // }
        // 
        // public string getErrorTitle()
        // {
        //     if (!hasError())
        //     {
        //         return "";
        //     }
        //     String title = "";
        //     title += fecha;
        //     title += " - " + sociedad;
        //     if (!String.IsNullOrEmpty(fondo))
        //     {
        //         title += " - " + fondo;
        //     }
        //     return title;
        // }
    }
}
