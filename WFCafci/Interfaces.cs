﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace WFCafci
{
    public partial class Interfaces : Form
    {
        private List<Interfaz> selected;
        private List<X509Certificate2> certificates;

        private List<Interfaz> lstUnica;
        private List<Interfaz> filterUnica;

        private Boolean filtering;
        private Dictionary<string, dynamic> fondos;
        private Dictionary<string, string> sociedades;
        private Login ventanaPadre;
        private ErrorsView ventanaErrores;
        private EditarInterfaces ventanaEditarInterfaces;
        private bool canRefreshTable;
        private bool hasToRefreshTable;

        public Interfaces(Login ventanaPadre)
        {
            InitializeComponent();
            Helper.SetTitleVersion(this);
            versionName.Text += " " + Helper.VERSION;
            loadData();
            FormBorderStyle = FormBorderStyle.FixedSingle;
            this.ventanaPadre = ventanaPadre;
            this.ventanaErrores = new ErrorsView(this);
            this.ventanaEditarInterfaces = new EditarInterfaces(this);
            canRefreshTable = true;
            hasToRefreshTable = false;
            selected = new List<Interfaz>();
            /* Init the models */
            lstUnica = new List<Interfaz>();
            filtering = false;
            /* Init the gerente label */
            sociedadGerenteTag.Text = "";
            /* Init the pickers */
            timeFrom.CustomFormat = "dd/MM/yyyy";
            timeTo.CustomFormat = "dd/MM/yyyy";
            DateTime aWeekAgo = DateTime.Now.Subtract(TimeSpan.FromDays(7));
            timeFrom.Value = aWeekAgo;
            timeTo.Value = DateTime.Now;
            /* Get all the certificates */
            selectFirma.Items.Clear();
            selectFirma.Items.Add("Seleccione uno");
            selectFirma.SelectedIndex = 0;
            disableButton(btnError);
            disableButton(btnDownloadRecibo);
            certificates = Helper.getCertificates();
            foreach (X509Certificate2 cert in certificates)
            {
                string certificateName = null;
                try
                {
                    certificateName = Helper.getCertificateName(cert);
                }
                finally
                {
                    if (certificateName == null)
                    {
                        certificateName = "Certificado Sin Nombre";
                    }
                }
                selectFirma.Items.Add(certificateName);
            }
            if (certificates.Count > 0)
            {
                selectFirma.SelectedIndex = 1;
            }
            if (certificates.Count < 2)
            {
                selectFirma.Enabled = false;
            }
            Helper.AdjustWidthComboBox_DropDown(selectFirma);
            this.ClientSize = new System.Drawing.Size(1010, 710);
            this.StartPosition = FormStartPosition.CenterScreen;

            //Label en rojo agregado para develop y staging.
            var modoComp = Helper.GetModoCompilacion();
            lblAtencion.Visible =   modoComp != Helper.ModoCompilacion.Prod;
            if (modoComp != Helper.ModoCompilacion.Staging)
                lblAtencion.Text = modoComp.ToString().ToUpper();
        }

        private void disableButton(Button btn)
        {
            btn.Enabled = false;
            btn.BackColor = System.Drawing.Color.LightGray;
            btn.Cursor = Cursors.No;
        }

        private void enableButton(Button btn)
        {
            btn.Enabled = true;
            btn.BackColor = btn.FlatAppearance.MouseOverBackColor;
            btn.Cursor = Cursors.Default;
        }

        /**
         * Get copy of selection to avoid crashes
         **/
        private List<Interfaz> getSelected()
        {
            List<Interfaz> selectCopy = null;
            try
            {
                selectCopy = new List<Interfaz>(selected);
            } catch (Exception e)
            {
                Console.WriteLine("Error on coping list: {0}", e);
                selectCopy = new List<Interfaz>();
            }
            return selectCopy;
        }
        private async void loadData()
        {
            try
            {
                getFolder.Enabled = false;
                statusTextBox.Text = "Cargando información";
                ApiHelper.setupWebSocket(this);
                currentFolder.Text = Properties.Settings.Default.xmlPath;
                currentFolder.Update();
                dynamic sociedadGerente = await ApiHelper.getSociedadGerente();
                sociedadGerenteTag.Text = sociedadGerente.nombre;
                statusTextBox.Text = "Cargando Sociedad Gerente";
                fondos = await ApiHelper.getFondos(sociedadGerente.id.ToString());
                statusTextBox.Text = "Cargando Fondos";
                sociedades = await ApiHelper.getSociedades(fondos["sociedades"]);
                statusTextBox.Text = "Cargando Sociedades";
                getFolder.Enabled = true;
                if (!String.IsNullOrEmpty(currentFolder.Text))
                {
                    generateInterfacesForPath(currentFolder.Text);
                }
                statusTextBox.Text = "Listo";
            } catch(Exception e)
            {
                Console.WriteLine("Silent fail on data error: {0}", e);
                fondos = null;
                sociedades = null;
                statusTextBox.Text = "Error";
            }

        }

        private void getFolderClick(object sender, EventArgs e)
        {
            // Show the FolderBrowserDialog.
            DialogResult result = folderBrowser.ShowDialog();
            if (result == DialogResult.OK)
            {
                Properties.Settings.Default.xmlPath = folderBrowser.SelectedPath;
                Properties.Settings.Default.Save();
                currentFolder.Text = folderBrowser.SelectedPath;
                currentFolder.Update();
                generateInterfacesForPath(currentFolder.Text);   
            }
        }

        private void reloadFiles(object sender, EventArgs e)
        {
            ApiHelper.cancelRunningTasks();
            generateInterfacesForPath(currentFolder.Text);
        }

        private bool isValidFilename(string filename)
        {
            Regex filenameRegex = new Regex(@"\\I_CAFCI_(\d{4})(\d{4})?_(\d+)(_N?\d+)?\.xml$");
            return (filenameRegex.IsMatch(filename));
        }

        private void generateInterfacesForPath(String currentPath)
        {

            statusTextBox.Text = "Buscando Interfaces";
            bool foundInterfazUnica = false;

            List<string> files = Helper.walkPath(currentPath);

            /* Fill the data grid */
            lstUnica.Clear();

            foreach (string file in files)
            {
                if (!isValidFilename(file))
                {
                    continue;
                }

                Interfaz interfaz = new Interfaz(this, file, sociedades, fondos);

                switch (interfaz.tipo)
                {
                    case "Unica":
                        if (interfaz.isSameSociedad(sociedadGerenteTag.Text))
                        {
                            lstUnica.Add(interfaz);
                            foundInterfazUnica = true;
                        }
                        break;
                    default:
                        Console.WriteLine("Invalid type for Path: {0}", file);
                        break;
                }
            }

            filtering = false;
            tablaUnica.DataSource = null;
            tablaUnica.DataSource = lstUnica;
            tablaUnica.ClearSelection();
            // Set default styles
            tablaUnica.AutoGenerateColumns = false;
            tablaUnica.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            tablaUnica.Columns[0].Width = 210;
            tablaUnica.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            tablaUnica.Columns[1].Width = 70;
            tablaUnica.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            tablaUnica.Columns[3].Width = 110;
            tablaUnica.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            tablaUnica.Columns[4].Width = 120;
            tablaUnica.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            tablaUnica.Columns[5].Width = 80;
            selected.Clear();

            statusTextBox.Text = "Listo";
            if (!foundInterfazUnica)
            {
                MessageBox.Show("No se encontraron archivos de interfaz en la ruta seleccionada.");
            }
            else
            {
                // Only show interfaces that are a week old or less
                onTimeChange(null, null);
            }
            

        }

        private void onTimeChange(object sender, EventArgs e)
        {
            filterUnica = filterList(lstUnica, timeFrom.Value, timeTo.Value);
            tablaUnica.DataSource = null;
            tablaUnica.DataSource = filterUnica;
            tablaUnica.ClearSelection();
            selected.Clear();
            filtering = true;
        }

        private List<Interfaz> filterList(List<Interfaz> list, DateTime from, DateTime to)
        {
            from = removeTime(from);
            to = removeTime(to);
            List<Interfaz> filtered = new List<Interfaz>();
            foreach (Interfaz item in list)
            {
                if (item.date >= from && item.date <= to)
                {
                    filtered.Add(item);
                }
            }
            return filtered;
        }

        private const string dateOnlyFormat = "yyyyMMdd";

        private DateTime removeTime(DateTime origin)
        {
            return DateTime.ParseExact(origin.ToString(dateOnlyFormat), dateOnlyFormat,
                                       System.Globalization.CultureInfo.InvariantCulture);
        }

        private void onTabChange(object sender, EventArgs e)
        {
            tablaUnica.ClearSelection();
            selected.Clear();
        }

        private void fileSelectionChanged(object table, EventArgs e)
        {
            // Select current tab list
            List<Interfaz> currentList;
            switch (tabs.SelectedIndex)
            {
                case 0:
                    if (filtering)
                    {
                        currentList = filterUnica;
                    }
                    else
                    {
                        currentList = lstUnica;
                    }
                    break;
                default:
                    MessageBox.Show("Error: tab index incorrecto!!!");
                    Console.WriteLine("Error: tab index incorrecto!!!");
                    return;
            }
            // Get all the models of the selected rows
            selected.Clear();
            disableButton(btnError);
            disableButton(btnDownloadRecibo);
            enableButton(btnSendSelection);

            enableButton(btnEditar);
            if (((DataGridView)table).SelectedRows.Count > 1) {
                disableButton(btnEditar);
            }

            bool disableDownloadRecibo = false;
            bool disableSendSelection = false;
            foreach (DataGridViewRow row in ((DataGridView) table).SelectedRows)
            {
                if (currentList != null && row.Index >= 0 && row.Index < currentList.Count)
                {
                    var interfaz = currentList[row.Index];
                    if (!interfaz.enviando)
                    {
                        enableButton(btnError);
                        if (!interfaz.hayError)
                        {
                            enableButton(btnDownloadRecibo);
                        }
                    }
                    else 
                    {
                        disableSendSelection = true;
                        disableDownloadRecibo = true;
                        disableButton(btnEditar);
                    }

                    if (!interfaz.allowEditing())
                    {
                        disableButton(btnEditar);
                    }

                    string referencia = interfaz.referencia;
                    Regex alreadySentRegex = new Regex(@"N\d+$");
                    if (referencia != null && alreadySentRegex.IsMatch(referencia))
                    {
                        enableButton(btnDownloadRecibo);
                    } else
                    {
                        disableDownloadRecibo = true;
                    }

                    if (disableSendSelection)
                    {
                        disableButton(btnSendSelection);
                    }

                    if (disableDownloadRecibo)
                    {
                        disableButton(btnDownloadRecibo);
                    }

                    selected.Add(interfaz);
                }
            }
        }

        private void btnSendSelection_Click(object sender, EventArgs e)
        {
            // If no certificate cry y.y
            if (selectFirma.SelectedIndex == 0)
            {
                MessageBox.Show("Primero seleccione una firma");
                return;
            }

            try
            {
                // Get current certificate
                X509Certificate2 cert = certificates[selectFirma.SelectedIndex - 1];
                ApiHelper.csp = (RSACryptoServiceProvider)cert.PrivateKey;

                if (ApiHelper.csp == null)
                {
                    MessageBox.Show("La firma seleccionada es invalida. Por favor elija otra.");
                    return;
                }

                if (selected == null || selected.Count <= 0)
                {
                    MessageBox.Show("Primero seleccione al menos una interfaz");
                    return;
                }

                bool isInvalid = false;
                canRefreshTable = false;
                foreach (Interfaz interfaz in getSelected())
                {
                    switch (interfaz.tipo)
                    {
                        case "Unica":
                            ApiHelper.completeAndSendInterfazUnica(interfaz);
                            break;
                        default:
                            isInvalid = true;
                            break;
                    }
                }
                if (isInvalid)
                {
                    MessageBox.Show("Esta interfaz no esta implementada actualmente");
                }

                canRefreshTable = true;
                if (hasToRefreshTable)
                {
                    updateView(this.tablaUnica);
                    hasToRefreshTable = false;
                    }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Esta firma está en el formato incorrecto. Exportarla a formato RSA.");
            }
        }

        private void btnError_Click(object sender, EventArgs e)
        {
            if (selected == null || selected.Count <= 0)
            {
                MessageBox.Show("Primero seleccione una interfaz");
                return;
            }
            try
            {
                this.ventanaErrores.setInterfacesToShow(getSelected());
                this.ventanaErrores.ShowDialog();
            } catch (Exception excp)
            {
                if (excp.Message == "no-errors")
                {
                    MessageBox.Show("No se han registrado avisos / errores en las interfaces seleccionadas.");
                }
                else
                {
                    MessageBox.Show("Hubo un error inesperado.");
                }
            }
        }

        private void tabs_Selecting(object sender, TabControlCancelEventArgs e)
        {
            e.Cancel = !e.TabPage.Enabled;
        }

        private void goToCMS(object sender, EventArgs e)
        {
            ApiHelper.goToCMS();
        }

        private bool logoutInProgress = false; 
        
        private async void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            if (this.logoutInProgress)
            {
                return;
            }
            else
            {
                this.logoutInProgress = true;
            }
            try
            {
                string error = await ApiHelper.logout();
                if (String.IsNullOrEmpty(error))
                {
                    this.Close();
                }
                else
                {
                    MessageBox.Show(ApiHelper.translateError(error, true));
                }
                this.logoutInProgress = false;
            } catch (Exception excp)
            {
                this.Close();
            }
        }

        // Updates views for tablas
        private void updateView(DataGridView tabla)
        {
            List<Interfaz> data = (List<Interfaz>) tabla.DataSource;
            tabla.DataSource = null;
            tabla.DataSource = data;
        }

        public void updateViewTabla(string tipo)
        {
            if (!canRefreshTable)
            {
                hasToRefreshTable = true;
                return;
            }
            switch (tipo)
            {
                case "Unica":
                    this.updateView(this.tablaUnica);
                    break;
            }
        }

        private void btnDownloadRecibo_Click(object sender, EventArgs e)
        {
            if (selected == null || selected.Count <= 0)
            {
                MessageBox.Show("Primero seleccione al menos una interfaz.");
                return;
            }

            canRefreshTable = false;
            foreach (Interfaz interfaz in getSelected())
            {
                ApiHelper.getAcuseRecibo(interfaz);
            }

            canRefreshTable = true;
            if (hasToRefreshTable)
            {
                updateView(this.tablaUnica);
                hasToRefreshTable = false;
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (selected == null || selected.Count <= 0)
            {
                MessageBox.Show("Primero seleccione una interfaz");
                return;
            }
            try
            {
                this.ventanaEditarInterfaces.setInterfacesToShow(getSelected());
                this.ventanaEditarInterfaces.ShowDialog();
            }
            catch (Exception excp)
            {
                MessageBox.Show("Hubo un error inesperado.");
            }
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}