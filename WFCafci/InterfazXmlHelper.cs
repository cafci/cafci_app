﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace WFCafci
{
    class InterfazXmlHelper
    {
        public class InterfazXmlData
        {
            public class CarterasXmlData
            {
                public class ActivoXmlData
                {
                    public string orden;
                    public string clase;
                    public string subclase;
                    public string entidadEmisoraNombre;
                    public string entidadOrganizadoraNombre;
                    public string activoNombre;
                    public string tipoTicker;
                    public string ticker;
                    public string mercadoNombre;
                    public string mercadoTipoCodigo;
                    public string mercadoCodigo;
                    public string monedaNombre;
                    public string monedaTipoCodigo;
                    public string monedaCodigo;
                }

                public string duration;
                public Dictionary<string, ActivoXmlData> activos;
            }

            public CarterasXmlData carteras;
        } 

        private static InterfazXmlData.CarterasXmlData readCarteras(XDocument xmlInterfaz)
        {
            XElement carterasElement = xmlInterfaz.Root.Element("CarterasInversiones");
            if (carterasElement != null)
            {
                InterfazXmlData.CarterasXmlData carteras = new InterfazXmlData.CarterasXmlData();
                XElement durationsElement = carterasElement.Element("Durations");
                if (durationsElement != null)
                {
                    XElement durationElement = durationsElement.Element("Duration");
                    carteras.duration = durationElement != null ? durationElement.Value : null;
                }

                IEnumerable<XElement> activosElements = carterasElement.Element("Activos").Elements("Activo");

                carteras.activos = new Dictionary<string, InterfazXmlData.CarterasXmlData.ActivoXmlData>();
                foreach (XElement activoElement in activosElements)
                {
                    string key = activoElement.Element("Nro").Value;
                    InterfazXmlData.CarterasXmlData.ActivoXmlData activo = new InterfazXmlData.CarterasXmlData.ActivoXmlData();

                    activo.orden = activoElement.Element("Nro").Value;
                    activo.clase = activoElement.Element("Cl").Value;
                    activo.subclase = activoElement.Element("SCl").Value;
                    activo.entidadEmisoraNombre = activoElement.Element("EntENom").Value;
                    activo.entidadOrganizadoraNombre = activoElement.Element("EntONom").Value;
                    activo.activoNombre = activoElement.Element("ActNom").Value;
                    activo.tipoTicker = activoElement.Element("ActTTic").Value;
                    activo.ticker = activoElement.Element("ActTic").Value;
                    activo.mercadoNombre = activoElement.Element("MerNom").Value;
                    activo.mercadoTipoCodigo = activoElement.Element("MerTCod").Value;
                    activo.mercadoCodigo = activoElement.Element("MerCod").Value;
                    activo.monedaNombre = activoElement.Element("MonActNom").Value;
                    activo.monedaTipoCodigo = activoElement.Element("MonActTCod").Value;
                    activo.monedaCodigo = activoElement.Element("MonActCod").Value;

                    carteras.activos.Add(key, activo);
                }

                return carteras;
            }

            return null;
        }

        public static InterfazXmlData readInterfaz(string filepath)
        {
            InterfazXmlData readData = new InterfazXmlData();
            try
            {
                XDocument xmlInterfaz = XDocument.Load(filepath);
                readData.carteras = readCarteras(xmlInterfaz);
            }
            catch (Exception e)
            {
                readData.carteras = null;
                Console.WriteLine("Error on read interfaz: {0}",e);
            }
            return readData;
        }

        public static void writeCarteras(XDocument xmlInterfaz, InterfazXmlData data)
        {
            XElement carterasElement = xmlInterfaz.Root.Element("CarterasInversiones");
            if (carterasElement != null)
            {
                XElement durationsElement = carterasElement.Element("Durations");
                if (durationsElement == null)
                {
                    carterasElement.Add(new XElement("Durations"));
                    durationsElement = carterasElement.Element("Durations");
                }
                XElement durationElement = durationsElement.Element("Duration");
                if (durationElement == null)
                {
                    durationsElement.SetElementValue("Duration", data.carteras.duration);
                } else
                {
                    durationElement.SetValue(data.carteras.duration);
                }
            }
        }

        public static void writeInterfaz(string filepath, InterfazXmlData data)
        {
            XDocument xmlInterfaz = XDocument.Load(filepath);
            writeCarteras(xmlInterfaz, data);
            xmlInterfaz.Save(filepath);
        }
    }
}
