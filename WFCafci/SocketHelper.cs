﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Web;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using Quobject.SocketIoClientDotNet.Client;

namespace WFCafci
{
    class SocketHelper
    {
        // Custom classes
        public enum SocketHelperStatus
        {
            OPENING,
            OPEN,
            CLOSED
        };

        public class SocketMessageEventArgs : EventArgs
        {
            public string Event { get; private set; }
            public dynamic Data { get; private set; }

            // Constructor. 
            public SocketMessageEventArgs(string e, dynamic data)
            {
                this.Event = e;
                this.Data = data;
            }
        }
        public delegate void SocketMessageEventHandler(object sender, SocketMessageEventArgs e);

        // Params
        private String baseUrl;
        private Socket webSocket;
        public SocketHelperStatus status { get; private set; } = SocketHelperStatus.OPENING;

        // Events
        public event EventHandler onOpen;
        public event EventHandler onClose;
        public event SocketMessageEventHandler onMessage;

        public SocketHelper(string url, ApiToken token)
        {
            this.baseUrl = url;
            changeToken(token);
        }

        public void changeToken(ApiToken token)
        {
            // Close old socket (if any)
            if (webSocket != null)
            {
                webSocket.Close();
                webSocket = null;
            }
            // Cannot init without token
            if (token == null || String.IsNullOrEmpty(token.accessToken))
            {
                throw new Exception("Connect first");
            }
            status = SocketHelperStatus.OPENING;
            // Configure socket
            IO.Options opts = new IO.Options();
            opts.Query = new Dictionary<string, string>();
            opts.Query.Add("token", ApiHelper.token.accessToken);
            opts.Reconnection = false;
            opts.AutoConnect = false;

            //Agregar custom headers para identificacion del lado del API
            opts.ExtraHeaders.Add("Origin", Helper.ORIGEN);

            webSocket = IO.Socket(baseUrl, opts);
            webSocket.On(Socket.EVENT_CONNECT, () =>
            {
                status = SocketHelperStatus.OPEN;
                if (this.onOpen != null)
                {
                    this.onOpen(this, null);
                }
            });
            webSocket.On(Socket.EVENT_ERROR, () =>
            {
                status = SocketHelperStatus.CLOSED;
                if (this.onClose != null)
                {
                    this.onClose(this, null);
                }
            });
            webSocket.On(Socket.EVENT_CONNECT_ERROR, () =>
            {
                status = SocketHelperStatus.CLOSED;
                if (this.onClose != null)
                {
                    this.onClose(this, null);
                }
            });
            webSocket.On(Socket.EVENT_CONNECT_TIMEOUT, () =>
            {
                status = SocketHelperStatus.CLOSED;
                if (this.onClose != null)
                {
                    this.onClose(this, null);
                }
            });
            webSocket.On(Socket.EVENT_DISCONNECT, () =>
            {
                status = SocketHelperStatus.CLOSED;
                if (this.onClose != null)
                {
                    this.onClose(this, null);
                }
            });
            // Expected messages
            webSocket.On("end", (data) => {
                onMessageFire("end", data);
            });
            webSocket.On("completarResult", (data) => {
                onMessageFire("completarResult", data);
            });
            webSocket.On("guardarResult", (data) => {
                onMessageFire("guardarResult", data);
            });
            // Just connect
            webSocket.Connect();
        }

        private void onMessageFire(string socket, object data)
        {
            if (this.onMessage != null)
            {
                SocketMessageEventArgs args = new SocketMessageEventArgs(socket, (dynamic)data);
                this.onMessage(this, args);
            }
        }

        public void send(string message, string data)
        {
            var sendPackage = new MethodInvoker(delegate {
                webSocket.Emit(message, data);
            });
            if (status == SocketHelperStatus.OPEN)
            {
                sendPackage.Invoke();
            } else
            {
                EventHandler handler = null;
                EventHandler closeHandler = null;
                handler = (err, datas) => {
                    onOpen -= handler;
                    onClose -= closeHandler;
                    sendPackage.Invoke();
                };
                closeHandler = (err, datas) => {
                    onOpen -= handler;
                    onClose -= closeHandler;
                };
                onOpen += handler;
                onClose += closeHandler;
                if (status == SocketHelperStatus.CLOSED)
                {
                    status = SocketHelperStatus.OPENING;
                    webSocket.Connect();
                }
            }
        }


        /*
         // Socket attributes.
         private string baseUrl;
         //private WebSocket webSocket;
         private EventHandler onOpen;

         public SocketHelper(string url)
         {
             // Socket initialization.
             baseUrl = url;
             buildWebSocket();
         }

         private async void buildWebSocket()
         {
             string url = await buildUrl();
             webSocket = new WebSocket(url);

             webSocket.OnOpen += handleSocketOpen;
             webSocket.OnMessage += handleSocketMessage;
             webSocket.OnError += handleSocketError;
             webSocket.OnClose += handleSocketClose;


             webSocket.ConnectAsync();
         }

         private async Task<string> buildUrl()
         {
             AuthenticationHeaderValue token = await ApiHelper.getAuthenticationHeader();
             var auxToken = HttpUtility.UrlEncode(token.ToString());
             return baseUrl + "/socket.io/?EIO=2&transport=websocket&token=" + auxToken;
         }

         private void handleSocketOpen(Object sender, EventArgs e)
         {
             Task.Run(() => {
                 while (webSocket.ReadyState == WebSocketState.Open || webSocket.ReadyState == WebSocketState.Connecting)
                 {
                     this.send("keepalive", "keepalive");
                     Thread.Sleep(5000);
                 }
             });
         }

         private void handleSocketClose(object sender, CloseEventArgs e)
         {
             onClose(this, null);
             Console.WriteLine("Socket closed: " + e.Reason);
         }

         private void handleSocketError(object sender, ErrorEventArgs e)
         {
             webSocket.Close();
             onClose(this, null);
             Console.WriteLine("Error on socket: " + e.Message);
         }

         private void handleSocketMessage(object sender, MessageEventArgs e)
         {
             // Socket is ready on socket.io end.
             if (e.Data.Substring(0, 2) == "40")
             {
                 if (onOpen != null)
                 {
                     onOpen(this, null);
                 }
             }
             // Socket is closed ??? on socket.io end.
             else if (e.Data.Substring(0, 2) == "44")
             {
                 webSocket.Close();
             }
             // Ignore first message sent by socket since it is internal to the socket.io protocol.
             else if (e.Data.Substring(0,1) != "0")
             {
                 SocketMessageEventArgs messageArgs = interpretPacket(e.Data);
                 onMessage(this, messageArgs);
             }
         }

         public void send(string e, string data)
         {
             var sendPackage = new MethodInvoker(delegate{
                 string packet = buildPacket(e, data);
                 webSocket.SendAsync(packet, null);
             });

             if (webSocket.ReadyState == WebSocketState.Open)
             {
                 sendPackage.Invoke();
             } else
             {
                 EventHandler handler = null;
                 handler = (err, datas) => {
                     onOpen -= handler;
                     sendPackage.Invoke();
                 };
                 onOpen += handler;
                 if (webSocket.ReadyState == WebSocketState.Closed)
                 {
                     webSocket.ConnectAsync();
                 }
             }
         }

         public class SocketMessageEventArgs : EventArgs
         {
             public string Event { get; private set; }
             public string Data { get; private set; }

             // Constructor. 
             public SocketMessageEventArgs(string e, string data)
             {
                 this.Event = e;
                 this.Data = data;
             }
         }

         public delegate void SocketMessageEventHandler(object sender, SocketMessageEventArgs e);

         public event SocketMessageEventHandler onMessage;

         public event EventHandler onClose;

         private string buildPacket(string e, string data)
         {
             return "42" + JsonConvert.SerializeObject(new[] { e, data });
         }

         private SocketMessageEventArgs interpretPacket(string packet)
         {
             int start = packet.IndexOf('"') + 1;
             string e = packet.Substring(start, packet.IndexOf('"', start) - start);
             start = packet.IndexOf(',') + 1;
             string data = packet.Substring(start, packet.LastIndexOf(']') - start);
             return new SocketMessageEventArgs(e, data);            
         }
         */
    }
}
