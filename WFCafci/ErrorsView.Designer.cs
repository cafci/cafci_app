﻿namespace WFCafci
{
    partial class ErrorsView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorsView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.returnButton = new System.Windows.Forms.Button();
            this.tablaErrores = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.titleImage = new System.Windows.Forms.PictureBox();
            this.separatorHeader = new System.Windows.Forms.Label();
            this.tabs = new System.Windows.Forms.TabControl();
            this.Errores = new System.Windows.Forms.TabPage();
            this.Warnings = new System.Windows.Forms.TabPage();
            this.tablaWarnings = new System.Windows.Forms.DataGridView();
            this.ErroresRecibo = new System.Windows.Forms.TabPage();
            this.tablaErroresRecibo = new System.Windows.Forms.DataGridView();
            this.detallesErrorBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tablaErrores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.titleImage)).BeginInit();
            this.tabs.SuspendLayout();
            this.Errores.SuspendLayout();
            this.Warnings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablaWarnings)).BeginInit();
            this.ErroresRecibo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablaErroresRecibo)).BeginInit();
            this.SuspendLayout();
            // 
            // returnButton
            // 
            this.returnButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(142)))), ((int)(((byte)(173)))));
            this.returnButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.returnButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.returnButton.ForeColor = System.Drawing.Color.White;
            this.returnButton.Location = new System.Drawing.Point(867, 33);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(98, 27);
            this.returnButton.TabIndex = 2;
            this.returnButton.Text = "Volver";
            this.returnButton.UseVisualStyleBackColor = false;
            this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // tablaErrores
            // 
            this.tablaErrores.AllowUserToAddRows = false;
            this.tablaErrores.AllowUserToDeleteRows = false;
            this.tablaErrores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tablaErrores.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.tablaErrores.BackgroundColor = System.Drawing.Color.White;
            this.tablaErrores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tablaErrores.DefaultCellStyle = dataGridViewCellStyle1;
            this.tablaErrores.GridColor = System.Drawing.Color.White;
            this.tablaErrores.Location = new System.Drawing.Point(3, 3);
            this.tablaErrores.Name = "tablaErrores";
            this.tablaErrores.ReadOnly = true;
            this.tablaErrores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tablaErrores.Size = new System.Drawing.Size(944, 559);
            this.tablaErrores.TabIndex = 3;
            this.tablaErrores.SelectionChanged += new System.EventHandler(this.fileSelectionChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(246, 20);
            this.label5.TabIndex = 22;
            this.label5.Text = "Listado de Errores y Advertencias";
            // 
            // titleImage
            // 
            this.titleImage.Image = ((System.Drawing.Image)(resources.GetObject("titleImage.Image")));
            this.titleImage.Location = new System.Drawing.Point(56, 12);
            this.titleImage.Name = "titleImage";
            this.titleImage.Size = new System.Drawing.Size(170, 60);
            this.titleImage.TabIndex = 23;
            this.titleImage.TabStop = false;
            // 
            // separatorHeader
            // 
            this.separatorHeader.BackColor = System.Drawing.Color.Black;
            this.separatorHeader.Location = new System.Drawing.Point(-1, 75);
            this.separatorHeader.Name = "separatorHeader";
            this.separatorHeader.Size = new System.Drawing.Size(1024, 2);
            this.separatorHeader.TabIndex = 24;
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.Errores);
            this.tabs.Controls.Add(this.Warnings);
            this.tabs.Controls.Add(this.ErroresRecibo);
            this.tabs.Location = new System.Drawing.Point(25, 109);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(958, 554);
            this.tabs.TabIndex = 25;
            // 
            // Errores
            // 
            this.Errores.Controls.Add(this.tablaErrores);
            this.Errores.Location = new System.Drawing.Point(4, 22);
            this.Errores.Name = "Errores";
            this.Errores.Size = new System.Drawing.Size(950, 528);
            this.Errores.TabIndex = 3;
            this.Errores.Text = "Errores";
            this.Errores.UseVisualStyleBackColor = true;
            // 
            // Warnings
            // 
            this.Warnings.Controls.Add(this.tablaWarnings);
            this.Warnings.Location = new System.Drawing.Point(4, 22);
            this.Warnings.Name = "Warnings";
            this.Warnings.Size = new System.Drawing.Size(950, 528);
            this.Warnings.TabIndex = 4;
            this.Warnings.Text = "Advertencias";
            this.Warnings.UseVisualStyleBackColor = true;
            // 
            // tablaWarnings
            // 
            this.tablaWarnings.AllowUserToAddRows = false;
            this.tablaWarnings.AllowUserToDeleteRows = false;
            this.tablaWarnings.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tablaWarnings.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.tablaWarnings.BackgroundColor = System.Drawing.Color.White;
            this.tablaWarnings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tablaWarnings.DefaultCellStyle = dataGridViewCellStyle2;
            this.tablaWarnings.GridColor = System.Drawing.Color.White;
            this.tablaWarnings.Location = new System.Drawing.Point(3, 3);
            this.tablaWarnings.Name = "tablaWarnings";
            this.tablaWarnings.ReadOnly = true;
            this.tablaWarnings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tablaWarnings.Size = new System.Drawing.Size(944, 562);
            this.tablaWarnings.TabIndex = 4;
            this.tablaWarnings.SelectionChanged += new System.EventHandler(this.fileSelectionChanged);
            // 
            // ErroresRecibo
            // 
            this.ErroresRecibo.Controls.Add(this.tablaErroresRecibo);
            this.ErroresRecibo.Location = new System.Drawing.Point(4, 22);
            this.ErroresRecibo.Name = "ErroresRecibo";
            this.ErroresRecibo.Size = new System.Drawing.Size(950, 528);
            this.ErroresRecibo.TabIndex = 5;
            this.ErroresRecibo.Text = "Errores Recibo";
            this.ErroresRecibo.UseVisualStyleBackColor = true;
            // 
            // tablaErroresRecibo
            // 
            this.tablaErroresRecibo.AllowUserToAddRows = false;
            this.tablaErroresRecibo.AllowUserToDeleteRows = false;
            this.tablaErroresRecibo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tablaErroresRecibo.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.tablaErroresRecibo.BackgroundColor = System.Drawing.Color.White;
            this.tablaErroresRecibo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.tablaErroresRecibo.DefaultCellStyle = dataGridViewCellStyle3;
            this.tablaErroresRecibo.GridColor = System.Drawing.Color.White;
            this.tablaErroresRecibo.Location = new System.Drawing.Point(3, 3);
            this.tablaErroresRecibo.Name = "tablaErroresRecibo";
            this.tablaErroresRecibo.ReadOnly = true;
            this.tablaErroresRecibo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tablaErroresRecibo.Size = new System.Drawing.Size(944, 559);
            this.tablaErroresRecibo.TabIndex = 5;
            this.tablaErroresRecibo.SelectionChanged += new System.EventHandler(this.fileSelectionChanged);
            // 
            // detallesErrorBtn
            // 
            this.detallesErrorBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            this.detallesErrorBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            this.detallesErrorBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.detallesErrorBtn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.detallesErrorBtn.ForeColor = System.Drawing.Color.White;
            this.detallesErrorBtn.Location = new System.Drawing.Point(843, 673);
            this.detallesErrorBtn.Name = "detallesErrorBtn";
            this.detallesErrorBtn.Size = new System.Drawing.Size(140, 27);
            this.detallesErrorBtn.TabIndex = 26;
            this.detallesErrorBtn.Text = "Ver más Detalles...";
            this.detallesErrorBtn.UseVisualStyleBackColor = false;
            this.detallesErrorBtn.Click += new System.EventHandler(this.detallesErrorBtn_Click);
            // 
            // ErrorsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1008, 712);
            this.Controls.Add(this.detallesErrorBtn);
            this.Controls.Add(this.tabs);
            this.Controls.Add(this.separatorHeader);
            this.Controls.Add(this.titleImage);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.returnButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ErrorsView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Interfaces";
            ((System.ComponentModel.ISupportInitialize)(this.tablaErrores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.titleImage)).EndInit();
            this.tabs.ResumeLayout(false);
            this.Errores.ResumeLayout(false);
            this.Warnings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tablaWarnings)).EndInit();
            this.ErroresRecibo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tablaErroresRecibo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.DataGridView tablaErrores;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox titleImage;
        private System.Windows.Forms.Label separatorHeader;
        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage Errores;
        private System.Windows.Forms.TabPage Warnings;
        private System.Windows.Forms.DataGridView tablaWarnings;
        private System.Windows.Forms.TabPage ErroresRecibo;
        private System.Windows.Forms.DataGridView tablaErroresRecibo;
        private System.Windows.Forms.Button detallesErrorBtn;
    }
}