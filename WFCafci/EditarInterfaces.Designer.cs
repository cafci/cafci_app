﻿namespace WFCafci
{
    partial class EditarInterfaces
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditarInterfaces));
            this.tablaEstructuraPrincipal = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tablaInfo = new System.Windows.Forms.TableLayoutPanel();
            this.fechaLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.fondoLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.sociedadLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.interfazLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tablaEditar = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.durationErrLabel = new System.Windows.Forms.Label();
            this.separatorHeader = new System.Windows.Forms.Label();
            this.titleImage = new System.Windows.Forms.PictureBox();
            this.returnButton = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.guardarButton = new System.Windows.Forms.Button();
            this.durationTextBox = new System.Windows.Forms.TextBox();
            this.tablaEstructuraPrincipal.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tablaInfo.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tablaEditar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.titleImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tablaEstructuraPrincipal
            // 
            this.tablaEstructuraPrincipal.ColumnCount = 1;
            this.tablaEstructuraPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tablaEstructuraPrincipal.Controls.Add(this.groupBox1, 0, 0);
            this.tablaEstructuraPrincipal.Controls.Add(this.groupBox2, 0, 1);
            this.tablaEstructuraPrincipal.Location = new System.Drawing.Point(12, 80);
            this.tablaEstructuraPrincipal.Name = "tablaEstructuraPrincipal";
            this.tablaEstructuraPrincipal.RowCount = 2;
            this.tablaEstructuraPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.57471F));
            this.tablaEstructuraPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.42529F));
            this.tablaEstructuraPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tablaEstructuraPrincipal.Size = new System.Drawing.Size(413, 174);
            this.tablaEstructuraPrincipal.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tablaInfo);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(407, 81);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // tablaInfo
            // 
            this.tablaInfo.ColumnCount = 2;
            this.tablaInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.93516F));
            this.tablaInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.06483F));
            this.tablaInfo.Controls.Add(this.fechaLabel, 1, 3);
            this.tablaInfo.Controls.Add(this.label9, 0, 3);
            this.tablaInfo.Controls.Add(this.fondoLabel, 1, 2);
            this.tablaInfo.Controls.Add(this.label5, 0, 2);
            this.tablaInfo.Controls.Add(this.sociedadLabel, 1, 1);
            this.tablaInfo.Controls.Add(this.label3, 0, 1);
            this.tablaInfo.Controls.Add(this.interfazLabel, 1, 0);
            this.tablaInfo.Controls.Add(this.label1, 0, 0);
            this.tablaInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tablaInfo.Location = new System.Drawing.Point(3, 16);
            this.tablaInfo.Name = "tablaInfo";
            this.tablaInfo.RowCount = 4;
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tablaInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tablaInfo.Size = new System.Drawing.Size(401, 62);
            this.tablaInfo.TabIndex = 4;
            // 
            // fechaLabel
            // 
            this.fechaLabel.AutoSize = true;
            this.fechaLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fechaLabel.Location = new System.Drawing.Point(106, 45);
            this.fechaLabel.Name = "fechaLabel";
            this.fechaLabel.Size = new System.Drawing.Size(292, 17);
            this.fechaLabel.TabIndex = 9;
            this.fechaLabel.Text = "2017-07-06";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(3, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(97, 17);
            this.label9.TabIndex = 8;
            this.label9.Text = "Fecha:";
            // 
            // fondoLabel
            // 
            this.fondoLabel.AutoSize = true;
            this.fondoLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fondoLabel.Location = new System.Drawing.Point(106, 30);
            this.fondoLabel.Name = "fondoLabel";
            this.fondoLabel.Size = new System.Drawing.Size(292, 15);
            this.fondoLabel.TabIndex = 5;
            this.fondoLabel.Text = "Galileo Event Driven";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Fondo:";
            // 
            // sociedadLabel
            // 
            this.sociedadLabel.AutoSize = true;
            this.sociedadLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sociedadLabel.Location = new System.Drawing.Point(106, 15);
            this.sociedadLabel.Name = "sociedadLabel";
            this.sociedadLabel.Size = new System.Drawing.Size(292, 15);
            this.sociedadLabel.TabIndex = 3;
            this.sociedadLabel.Text = "Galileo Argentina S.G.F.C.I.S.A.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Sociedad Gerente:";
            // 
            // interfazLabel
            // 
            this.interfazLabel.AutoSize = true;
            this.interfazLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.interfazLabel.Location = new System.Drawing.Point(106, 0);
            this.interfazLabel.Name = "interfazLabel";
            this.interfazLabel.Size = new System.Drawing.Size(292, 15);
            this.interfazLabel.TabIndex = 1;
            this.interfazLabel.Text = "SCC_IUnica_00010349_20170706_N002503";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Interfaz:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tablaEditar);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(3, 90);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(407, 81);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            // 
            // tablaEditar
            // 
            this.tablaEditar.ColumnCount = 2;
            this.tablaEditar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.87437F));
            this.tablaEditar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.12563F));
            this.tablaEditar.Controls.Add(this.splitContainer1, 1, 1);
            this.tablaEditar.Controls.Add(this.label11, 0, 1);
            this.tablaEditar.Controls.Add(this.durationErrLabel, 1, 0);
            this.tablaEditar.Location = new System.Drawing.Point(6, 18);
            this.tablaEditar.Name = "tablaEditar";
            this.tablaEditar.RowCount = 2;
            this.tablaEditar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.31343F));
            this.tablaEditar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 62.68657F));
            this.tablaEditar.Size = new System.Drawing.Size(398, 51);
            this.tablaEditar.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(3, 19);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 32);
            this.label11.TabIndex = 0;
            this.label11.Text = "Duration:";
            // 
            // durationErrLabel
            // 
            this.durationErrLabel.AutoSize = true;
            this.durationErrLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.durationErrLabel.ForeColor = System.Drawing.Color.Red;
            this.durationErrLabel.Location = new System.Drawing.Point(102, 0);
            this.durationErrLabel.Name = "durationErrLabel";
            this.durationErrLabel.Size = new System.Drawing.Size(293, 19);
            this.durationErrLabel.TabIndex = 2;
            this.durationErrLabel.Text = "No hay datos semanales para editar.";
            // 
            // separatorHeader
            // 
            this.separatorHeader.BackColor = System.Drawing.Color.Black;
            this.separatorHeader.Location = new System.Drawing.Point(-1, 75);
            this.separatorHeader.Name = "separatorHeader";
            this.separatorHeader.Size = new System.Drawing.Size(1024, 2);
            this.separatorHeader.TabIndex = 27;
            // 
            // titleImage
            // 
            this.titleImage.Image = ((System.Drawing.Image)(resources.GetObject("titleImage.Image")));
            this.titleImage.Location = new System.Drawing.Point(56, 12);
            this.titleImage.Name = "titleImage";
            this.titleImage.Size = new System.Drawing.Size(170, 60);
            this.titleImage.TabIndex = 26;
            this.titleImage.TabStop = false;
            // 
            // returnButton
            // 
            this.returnButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(142)))), ((int)(((byte)(173)))));
            this.returnButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.returnButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.returnButton.ForeColor = System.Drawing.Color.White;
            this.returnButton.Location = new System.Drawing.Point(318, 33);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(98, 27);
            this.returnButton.TabIndex = 25;
            this.returnButton.Text = "Volver";
            this.returnButton.UseVisualStyleBackColor = false;
            this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(102, 22);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.durationTextBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.guardarButton);
            this.splitContainer1.Size = new System.Drawing.Size(293, 26);
            this.splitContainer1.SplitterDistance = 194;
            this.splitContainer1.TabIndex = 28;
            this.splitContainer1.TabStop = false;
            // 
            // guardarButton
            // 
            this.guardarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(163)))), ((int)(((byte)(190)))), ((int)(((byte)(140)))));
            this.guardarButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guardarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.guardarButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.guardarButton.ForeColor = System.Drawing.Color.White;
            this.guardarButton.Location = new System.Drawing.Point(0, 0);
            this.guardarButton.Name = "guardarButton";
            this.guardarButton.Size = new System.Drawing.Size(95, 26);
            this.guardarButton.TabIndex = 29;
            this.guardarButton.Text = "Guardar";
            this.guardarButton.UseVisualStyleBackColor = false;
            this.guardarButton.Click += new System.EventHandler(this.guardarButton_Click);
            // 
            // durationTextBox
            // 
            this.durationTextBox.Location = new System.Drawing.Point(0, 0);
            this.durationTextBox.Name = "durationTextBox";
            this.durationTextBox.Size = new System.Drawing.Size(194, 20);
            this.durationTextBox.TabIndex = 2;
            // 
            // EditarInterfaces
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(437, 266);
            this.Controls.Add(this.separatorHeader);
            this.Controls.Add(this.titleImage);
            this.Controls.Add(this.returnButton);
            this.Controls.Add(this.tablaEstructuraPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "EditarInterfaces";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Editar";
            this.tablaEstructuraPrincipal.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tablaInfo.ResumeLayout(false);
            this.tablaInfo.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tablaEditar.ResumeLayout(false);
            this.tablaEditar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.titleImage)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tablaEstructuraPrincipal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tablaInfo;
        private System.Windows.Forms.Label fechaLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label fondoLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label sociedadLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label interfazLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label separatorHeader;
        private System.Windows.Forms.PictureBox titleImage;
        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tablaEditar;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label durationErrLabel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox durationTextBox;
        private System.Windows.Forms.Button guardarButton;
    }
}