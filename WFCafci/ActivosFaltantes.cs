﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFCafci
{
    public partial class ActivosFaltantes : Form
    {
        private ErrorsView ventanaPadre;

        public ActivosFaltantes(ErrorsView ventanaPadre)
        {
            InitializeComponent();
            Helper.SetTitleVersion(this);
            this.ventanaPadre = ventanaPadre;
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.ventanaPadre.Show();
        }

        public void setErrorToShow(dynamic error)
        {
            if (error.codigo == "CT0034")
            {
                InterfazXmlHelper.InterfazXmlData interfazData = InterfazXmlHelper.readInterfaz(error.path);
                var dataActivo = interfazData.carteras.activos[error.extraData];

                this.interfazLabel.Text = error.interfazFilename;
                this.ordenLabel.Text = dataActivo.orden;
                this.claseLabel.Text = dataActivo.clase;
                this.subclaseLabel.Text = dataActivo.subclase;
                this.entidadEmisoraLabel.Text = dataActivo.entidadEmisoraNombre;
                this.entidadOrganizadoraLabel.Text = dataActivo.entidadOrganizadoraNombre;
                this.nombreLabel.Text = dataActivo.activoNombre;
                this.tipoTickerLabel.Text = dataActivo.tipoTicker;
                this.tickerLabel.Text = dataActivo.ticker;
                this.mercadoLabel.Text = dataActivo.mercadoNombre;
                this.tipoCodigoMercadoLabel.Text = dataActivo.mercadoTipoCodigo;
                this.codigoMercadoLabel.Text = dataActivo.mercadoCodigo;
                this.monedaLabel.Text = dataActivo.monedaNombre;
                this.tipoCodigoMonedaLabel.Text = dataActivo.monedaTipoCodigo;
                this.codigoMonedaLabel.Text = dataActivo.monedaCodigo;

            } else
            {
                // TODO: handle error
            }
        }
    }
}
