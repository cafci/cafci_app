﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Net;

namespace WFCafci
{
    class ApiHelper
    {

        public static RSACryptoServiceProvider csp;
        public static String baseUrl;
        public static ApiToken token;
        private static String clientId;
        private static String clientSecret;
        private static String cmsClientId;
        private static String cmsClientSecret;
        private static String cmsUrl;

        private static string baseSocketUrl;
        private static bool subscribedToEvents = false;
        private static Dictionary<string, Interfaz> interfacesAwaitingResponse = new Dictionary<string, Interfaz>();
        private static SocketHelper socketHelper;

        public static void onInit()
        {
            //Sobreescribo las url para que no puedan ser cambiadas por config y siempre esten apuntando a staging.
            //baseUrl = Properties.Settings.Default.apiUrl;
            //baseSocketUrl = Properties.Settings.Default.apiSocketUrl;
            switch (Helper.GetModoCompilacion())
            {
                case Helper.ModoCompilacion.Prod:
                    baseUrl = "https://api.cafci.org.ar";
                    baseSocketUrl = "wss://api.cafci.org.ar";
                    cmsUrl = "https://admin.cafci.org.ar";
                    break;
                case Helper.ModoCompilacion.Staging:
                    baseUrl = "https://api.cafci2.org.ar";
                    baseSocketUrl = "wss://api.cafci2.org.ar";
                    cmsUrl = "https://admin.cafci2.org.ar";
                    break;
                case Helper.ModoCompilacion.Develop:
                    baseUrl = "https://api.dev.cafci2.org.ar/";
                    baseSocketUrl = "wss://api.dev.cafci2.org.ar/";
                    cmsUrl = "https://admin.dev.cafci2.org.ar";
                    break;
                default:
                    break;
            }

            token = null;
            csp = null;
            clientId = Properties.Settings.Default.clientId;
            clientSecret = Properties.Settings.Default.clientSecret;
            cmsClientId = Properties.Settings.Default.cmsClientId;
            cmsClientSecret = Properties.Settings.Default.cmsClientSecret;

            //cmsUrl = Properties.Settings.Default.cmsUrl;
            

            configureProxy();
        }

        /// <summary>
        /// Configures the proxy with the settings
        /// </summary>
        private static void configureProxy()
        {
            String user = Properties.Settings.Default.proxyUser;
            String pass = Properties.Settings.Default.proxyPassword;
            String domain = Properties.Settings.Default.proxyDomain;
            String uri = Properties.Settings.Default.proxyUrl;
            int port = Properties.Settings.Default.proxyPort;
            
            if (!String.IsNullOrEmpty(uri))
            {
                if (port < 0)
                {
                    port = 80;
                }
                Console.WriteLine("Proxy address is {0} and port is {1}", uri, port);
                WebRequest.DefaultWebProxy = new WebProxy(uri, port);
            }
            else if (WebRequest.DefaultWebProxy == null)
            {
                Console.WriteLine("Proxy not setup. Configure proxyUrl and proxyPort.");
                return;
            }

            if (String.IsNullOrEmpty(user) || String.IsNullOrEmpty(pass))
            {
                Console.WriteLine("Proxy credentials not setup. No proxy user or password available. User: {0} Pass: {1}", Properties.Settings.Default.proxyUser, pass);
                return;
            }
            if (String.IsNullOrEmpty(domain))
            {
                WebRequest.DefaultWebProxy.Credentials = new NetworkCredential(user, pass);
                Console.WriteLine("Proxy credentials setup without domain.");
            } else
            {
                WebRequest.DefaultWebProxy.Credentials = new NetworkCredential(user, pass, domain);
                Console.WriteLine("Proxy credentials setup with domain.");
            }
        }

        /// <summary>
        /// Translates the api error for humans to read it
        /// </summary>
        /// <param name="error">Error from the api</param>
        /// <returns>Human readable error</returns>
        public static string translateError(string error, bool fromLogin = false, string fileName = "")
        {
            string translatedError = "";
            string extraData = "";
            if (error.Contains("|"))
            {
                int separatorIndex = error.IndexOf("|");
                extraData = error.Substring(separatorIndex + 1);
                error = error.Substring(0, separatorIndex);
            }
            switch (error)
            {
                // v5.1 error codes
                case "T.0001":
                    translatedError = "T.0001: No se han encontrado archivos de interfaz.";
                    break;
                case "T.0002":
                    translatedError = "T.0002: La interfaz posee caracteres no válidos: "+ fileName;
                    break;
                case "T.0003":
                    translatedError = "T.0003: La estructura del archivo XML es incorrecta: "+ fileName;
                    break;
                case "T.0004":
                    translatedError = "T.0004: Existen tags incorrectos incluidos en el archivo XML: "+ fileName;
                    break;
                case "T.0005":
                    translatedError = "T.0005: El orden de los tags del archivo XML es incorrecto: "+ fileName;
                    break;
                case "T.0006":
                    translatedError = "T.0006: El formato de la información recibida es incorrecto: "+ extraData + " " + fileName;
                    break;
                case "T.0007":
                    translatedError = "T.0007: La Clase de Fondos no correspode a la Sociedad Gerente "+ extraData;
                    break;
                case "T.0008":
                    translatedError = "T.0008: El Fondo no corresponde a la Sociedad Gerente "+ extraData;
                    break;
                case "T.0009":
                    translatedError = "T.0009: El usuario ingresado no tiene relación con la Sociedad Gerente.";
                    break;
                case "T.0010":
                    translatedError = "T.0010: El valor \"fecha datos\" no es coincidente con la fecha de la interfaz.";
                    break;
                case "T.0011":
                    translatedError = "T.0011: El valor \"fecha datos\" no es válido.";
                    break;
                case "T.0012":
                    translatedError = "T.0012: El archivo fue firmado con una firma incorrecta.";
                    break;
                case "T.0013":
                    translatedError = "T.0013: ﻿Hubo un problema en las comunicaciones. Por favor reintente o comuníquese con la Cámara.";
                    break;
                case "T.0029":
                    translatedError = "T.0029: No se ha recibido información de Rendimiento Impositivo Diario para ID de la Clase: "+ extraData;
                    break;
                case "T.0030":
                    translatedError = "T.0030: La diferencia entre la suma del \"PN de las Clases\" y el \"PN Total del Fondo\" en la moneda del Fondo, excede el límite fijado.";
                    break;
                case "T.0031":
                    translatedError = "T.0031: El activo informado en el item número "+ extraData +" no se encuentra dado de alta en el \"Maestro de Activos\".";
                    break;
                case "T.0032":
                    translatedError = "T.0032: El dato requerido <"+extraData+"/> no se ha informado o es incorrecto.";
                    break;
                case "T.0033":
                    translatedError = "T.0033: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0034":
                    translatedError = "T.0034: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0035":
                    translatedError = "T.0035: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0036":
                    translatedError = "T.0036: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0037":
                    translatedError = "T.0037: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0038":
                    translatedError = "T.0038: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0039":
                    translatedError = "T.0039: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0040":
                    translatedError = "T.0040: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0041":
                    translatedError = "T.0041: No se ha recibido información de \"duration\" "+ extraData;
                    break;
                case "T.0042":
                    translatedError = "T.0042: El control de \"Activo Total - Pasivo Total = PN\" no es correto. ";
                    break;
                case "T.0043":
                    translatedError = "T.0043: La suma de los items que integran el \"Activo Total\" no coincide con lo informado. ";
                    break;
                case "T.0044":
                    translatedError = "T.0044: La suma de los items que integran el \"Pasivo Total\" no coincide con lo informado.";
                    break;
                case "T.0045":
                    translatedError = "T.0045: La información que se está enviando ya fue, recibida, procesada e informada a la CNV en anteriores despachos.";
                    break;
                case "T.0046":
                    translatedError = "T.0046: El monto en la moneda del activo informado en el ítem de cartera ("+ extraData +") no se corresponde con los valores de cantidad, cantidad de cotización y precio provistos. ";
                    break;
                case "T.0048":
                    translatedError = "T.0048: El activo informado en el item número "+ extraData +" no posee o es incorreta la información sobre \"Código Impositivo\". ";
                    break;
                case "T.0049":
                    translatedError = "T.0049: La cartera informada del Fondo "+ extraData +" no posee información sobre \"Cumplimiento de Activo Subyacente Principal\". ";
                    break;
                case "T.0050":
                    translatedError = "T.0050: El activo informado en el item número "+ extraData +"  no posee información sobre \"País Emisor\". ";
                    break;
                case "T.0051":
                    translatedError = "T.0051: El activo informado en el item número "+ extraData +"  no posee información sobre \"País Emisor\". ";
                    break;
                case "T.0052":
                    translatedError = "T.0052: El activo informado en el item número "+ extraData +" no posee información sobre \"País Negociado\". ";
                    break;
                case "T.0053":
                    translatedError = "T.0052: El activo informado en el item número "+ extraData +" no posee información sobre \"País Negociado\". ";
                    break;
                case "T.0143":
                    translatedError = "T.0143: Hay mas de un inversor con codigo TOTAL. ";
                    break;
                case "T.0144":
                    translatedError = "T.0144: No se informo el inversor con codigo TOTAL. ";
                    break;
                case "T.0145":
                    translatedError = "T.0145: El inversor de la posicion "+ extraData +" no indica cantidad. ";
                    break;
                case "T.0146":
                    translatedError = "T.0146: El inversor de la posicion "+ extraData +" no indica monto. ";
                    break;
                case "T.0147":
                    translatedError = "T.0147: La suma de los montos totales por tipo de inversor no coincide con el total general informado "+ extraData;
                    break;
                case "T.0148":
                    translatedError = "T.0148: La suma de las cantidades totales por tipo de inversor no coincide con el total general informado "+ extraData;
                    break;
                case "T.0149":
                    translatedError = "T.0149: La diferencia entre la cantidad total de personas físicas actual y anterior, excede el límite fijado "+ extraData;
                    break;
                case "T.0150":
                    translatedError = "T.0150: La diferencia entre la cantidad total de personas jurídicas actual y anterior, excede el límite fijado "+ extraData;
                    break;
                case "T.0151":
                    translatedError = "T.0151: La diferencia entre el monto total de personas físicas actual y anterior, excede el límite fijado "+ extraData;
                    break;
                case "T.0152":
                    translatedError = "T.0152: La diferencia entre el monto total de personas jurídicas actual y anterior, excede el límite fijado "+ extraData;
                    break;
                case "T.0153":
                    translatedError = "T.0153: No se ha recibido información para la siguiente Clase / Fondo. ID de la Clase: "+ extraData;
                    break;
                // v5.1 warning codes
                case "A.0014":
                    translatedError = "A.0014: El control de \"VCP * CCP = PN\", no es coincidente "+ extraData;
                    break;
                case "A.0015":
                    translatedError = "A.0015: La diferencia entre \"PN actual y PN anterior\", excede el límite fijado "+ extraData;
                    break;
                case "A.0016":
                    translatedError = "A.0016: La diferencia entre \"CCP actual y CCP anterior\", excede el límite fijado "+ extraData;
                    break;
                case "A.0017":
                    translatedError = "A.0017: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0018":
                    translatedError = "A.0018: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0019":
                    translatedError = "A.0019: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0020":
                    translatedError = "A.0020: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0021":
                    translatedError = "A.0021: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0022":
                    translatedError = "A.0022: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0023":
                    translatedError = "A.0023: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0024":
                    translatedError = "A.0024: El flujo diario de cantidad de cuotaparte no coincide con lo informado anteriormente. ";
                    break;
                case "A.0025":
                    translatedError = "A.0025: Se ha informado valor nulo para el PN "+ extraData;
                    break;
                case "A.0026":
                    translatedError = "A.0026: Se ha informado valor nulo para el VCP "+ extraData;
                    break;
                case "A.0027":
                    translatedError = "A.0027: Se ha informado valor nulo para el CCP "+ extraData;
                    break;
                case "A.0028":
                    translatedError = "A.0028: No se ha recibido información para la siguiente Clase / Fondo. ID de la Clase: "+ extraData;
                    break;
                case "A.0047":
                    translatedError = "A.0047: El valor \"fecha de vencimiento\" es menor que el valor \"fecha de datos\" "+ extraData;
                    break;
                case "A.0054":
                    translatedError = "A.0054: El activo que intenta dar de alta ya existe en el \"Maestro de Activos\": "+ extraData +". La solicitud de alta de este activo será ignorada.";
                    break;
                case "A.0055":
                    translatedError = "A.0055: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0056":
                    translatedError = "A.0055: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0057":
                    translatedError = "A.0057: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0058":
                    translatedError = "A.0058: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0059":
                    translatedError = "A.0059: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0060":
                    translatedError = "A.0060: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0061":
                    translatedError = "A.0061: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0062":
                    translatedError = "A.0062: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0063":
                    translatedError = "A.0063: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0064":
                    translatedError = "A.0064: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0065":
                    translatedError = "A.0065: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0066":
                    translatedError = "A.0066: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0067":
                    translatedError = "A.0067: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0068":
                    translatedError = "A.0068: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0069":
                    translatedError = "A.0069: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0070":
                    translatedError = "A.0070: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0071":
                    translatedError = "A.0071: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0072":
                    translatedError = "A.0072: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0073":
                    translatedError = "A.0073: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0074":
                    translatedError = "A.0074: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0075":
                    translatedError = "A.0075: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0076":
                    translatedError = "A.0076: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0077":
                    translatedError = "A.0077: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0078":
                    translatedError = "A.0078: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0079":
                    translatedError = "A.0079: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0080":
                    translatedError = "A.0080: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0081":
                    translatedError = "A.0081: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0082":
                    translatedError = "A.0082: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0083":
                    translatedError = "A.0083: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0084":
                    translatedError = "A.0084: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0085":
                    translatedError = "A.0085: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0086":
                    translatedError = "A.0086: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0087":
                    translatedError = "A.0087: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0088":
                    translatedError = "A.0088: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0089":
                    translatedError = "A.0089: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0090":
                    translatedError = "A.0090: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0091":
                    translatedError = "A.0091: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0092":
                    translatedError = "A.0092: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0093":
                    translatedError = "A.0093: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0094":
                    translatedError = "A.0094: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0095":
                    translatedError = "A.0095: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0096":
                    translatedError = "A.0096: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0097":
                    translatedError = "A.0097: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0098":
                    translatedError = "A.0098: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0099":
                    translatedError = "A.0099: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0100":
                    translatedError = "A.0100: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0101":
                    translatedError = "A.0101: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0102":
                    translatedError = "A.0102: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0103":
                    translatedError = "A.0103: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0104":
                    translatedError = "A.0104: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0105":
                    translatedError = "A.0105: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0106":
                    translatedError = "A.0106: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0107":
                    translatedError = "A.0107: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0108":
                    translatedError = "A.0108: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0109":
                    translatedError = "A.0109: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0110":
                    translatedError = "A.0110: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0111":
                    translatedError = "A.0111: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0112":
                    translatedError = "A.0112: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0113":
                    translatedError = "A.0113: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0114":
                    translatedError = "A.0114: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0115":
                    translatedError = "A.0115: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0116":
                    translatedError = "A.0116: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0117":
                    translatedError = "A.0117: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0118":
                    translatedError = "A.0118: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0119":
                    translatedError = "A.0119: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0120":
                    translatedError = "A.0120: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0121":
                    translatedError = "A.0121: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0122":
                    translatedError = "A.0122: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0123":
                    translatedError = "A.0123: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0124":
                    translatedError = "A.0124: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0125":
                    translatedError = "A.0125: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0126":
                    translatedError = "A.0126: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0127":
                    translatedError = "A.0127: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0128":
                    translatedError = "A.0128: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0129":
                    translatedError = "A.0129: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0130":
                    translatedError = "A.0130: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0131":
                    translatedError = "A.0131: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0132":
                    translatedError = "A.0132: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0133":
                    translatedError = "A.0133: El dato requerido no se ha informado o es incorrecto:"+ extraData;
                    break;
                case "A.0134":
                    translatedError = "A.0134: El dato requerido no se ha informado o es incorrecto:"+ extraData;
                    break;
                case "A.0135":
                    translatedError = "A.0135: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0136":
                    translatedError = "A.0136: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0137":
                    translatedError = "A.0137: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0138":
                    translatedError = "A.0138: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0139":
                    translatedError = "A.0139: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0140":
                    translatedError = "A.0140: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0141":
                    translatedError = "A.0141: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0142":
                    translatedError = "A.0142: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0154":
                    translatedError = "A.0154: El PN informado en \"Valores Diarios\", no coincide con el PN informado en el bloque \"Carteras Inversiones\". ";
                    break;
                case "A.0155":
                    translatedError = "A.0155: El PN informado en \"Valores Diarios\", no coincide con el PN informado en el bloque \"Inversores y Comisiones\". ";
                    break;
                // v5.2 error codes
                case "T.0154":
                    translatedError = "T.0154: El código de \"moneda\" no se encuentra dado de alta en el \"Maestro de Activos\" " + extraData;
                    break;
                case "T.0155":
                    translatedError = "T.0155: El código de \"mercado\" no se encuentra dado de alta en el \"Maestro de Activos\" " + extraData;
                    break;
                case "T.0165":
                    translatedError = "T.0165: La información recibida no tiene datos de \"valores diarios\", \"cartera de inversiones\" ni \"inversores y comisiones\"";
                    break;
                case "A.0159":
                    translatedError = "A.0159: El \"PN en la moneda del Fondo\" no coincide con el tipo de cambio informado en función del \"PN de la Clase\" " + extraData;
                    break;
                case "T.0158":
                    translatedError = "T.0158: El código de \"moneda\" no se encuentra dado de alta en el \"Maestro de Activos\" " + extraData;
                    break;
                case "T.0160":
                    translatedError = "T.0160: La información requerida para el activo no está completa " + extraData;
                    break;
                case "T.0161":
                    translatedError = "T.0161: El código de \"tipo de tasa\" no se encuentra dado de alta en el \"Maestro de Activos\" " + extraData;
                    break;
                case "T.0162":
                    translatedError = "T.0162: El código de \"tipo de cambio\" no se encuentra dado de alta en el \"Maestro de Activos\" " + extraData;
                    break;
                case "A.0163":
                    translatedError = "A.0163: El monto en la moneda del Fondo " + extraData;
                    break;
                case "A.0164":
                    translatedError = "A.0164: El código de \"tipo de cambio\" no se encuentra dado de alta en el \"Maestro de Activos\" " + extraData;
                    break;
                case "A.0156":
                    translatedError = "A.0156: El PN informado en \"Valores Diarios\", no coincide con el PN informado en el bloque \"Carteras Inversiones\".";
                    break;
                case "A.0157":
                    translatedError = "A.0157: El PN informado en \"Valores Diarios\", no coincide con el PN informado en el bloque \"Inversores y Comisiones\".";
                    break;
                // New error codes
                /* General Errors */
                case "GT0001":
                    translatedError = "GT0001: La Clase de Fondos no correspode a la Sociedad Gerente. ID de la Clase: "+ extraData;
                    break;
                case "GT0002":
                    translatedError = "GT0002: El Fondo no correspode a la Sociedad Gerente. ID del Fondo: "+ extraData;
                    break;
                case "GT0003":
                    translatedError = "GT0003: El usuario ingresado no tiene relación con la Sociedad Gerente.";
                    break;
                case "GT0005":
                    translatedError = "GT0005: El código de \"Sociedad Gerente\" no existe en la base de datos. ID de la Sociedad Gerente: "+ extraData;
                    break;
                case "GT0006":
                    translatedError = "GT0006: El valor \"fecha datos\" no es coincidente con la fecha de la interfaz.";
                    break;
                case "GT0007":
                    translatedError = "GT0007: El valor \"fecha datos\" no es válido.";
                    break;
                case "GT0008":
                    translatedError = "GT0008: La interfaz contiene caracteres inválidos.";
                    break;
                case "GT0012":
                    int separatorIndex = extraData.IndexOf("-");
                    string element = extraData.Substring(separatorIndex + 1);
                    string field = extraData.Substring(0, separatorIndex);
                    switch (field)
                    {
                        case "valorDiario.claseId":
                            translatedError += "Valor diario correspondiente a la clase " + element + ": ClaseId (id de la clase de fondo) tiene un formato incorrecto. Debe ser un número entero.";
                            break;
                        case "valorDiario.claseNombre":
                            translatedError += "Valor diario correspondiente a la clase " + element + ": ClaseNom (nombre de la clase de fondo) tiene un formato incorrecto.";
                            break;
                        case "valorDiario.monedaId":
                            translatedError += "Valor diario correspondiente a la clase " + element + ": MonId (id de la moneda) tiene un formato incorrecto. Debe ser un número entero.";
                            break;
                        case "valorDiario.monedaNombre":
                            translatedError += "Valor diario correspondiente a la clase " + element + ": MonNom (nombre de la moneda) tiene un formato incorrecto.";
                            break;
                        case "valorDiario.cantidadRescates":
                            translatedError += "Valor diario correspondiente a la clase " + element + ": CRes (cantidad de rescates) tiene un formato incorrecto. Debe ser un número con 20 enteros y 2 decimales.";
                            break;
                        case "valorDiario.cantidadSubscripciones":
                            translatedError += "Valor diario correspondiente a la clase " + element + ": CSus (cantidad de subscripciones) tiene un formato incorrecto.  Debe ser un número con 20 enteros y 2 decimales.";
                            break;
                        case "valorDiario.cantidadCuotapartes":
                            translatedError += "Valor diario correspondiente a la clase " + element + ": CCP (cantidad de cuotapartes) tiene un formato incorrecto.  Debe ser un número con 20 enteros y 2 decimales.";
                            break;
                        case "valorDiario.valor":
                            translatedError += "Valor diario correspondiente a la clase " + element + ": VCP (valor de cuotaparte) tiene un formato incorrecto. Debe ser un número con 20 enteros y 4 decimales.";
                            break;
                        case "valorDiario.patrimonioNeto":
                            translatedError += "Valor diario correspondiente a la clase " + element + ": PN (patrimonio neto) tiene un formato incorrecto. Debe ser un número con 20 enteros y 2 decimales.";
                            break;
                        case "valorDiario.patrimonioNetoFondo":
                            translatedError += "Valor diario correspondiente a la clase " + element + ": PNMFdo (patrimonio neto moneda fondo) tiene un formato incorrecto. Debe ser un número con 20 enteros y 2 decimales.";
                            break;
                        case "valorDiario.valorPesos":
                            translatedError += "Valor diario correspondiente a la clase " + element + ": VCPr (valor de cuotaparte en pesos) tiene un formato incorrecto. Debe ser un número con 20 enteros y 4 decimales.";
                            break;
                        case "determinacionCuotaparte.dividendosRentas":
                            translatedError += "Determinacion de cuotaparte: DivRentas (dividendos y rentas) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.creditoPorSubscripciones":
                            translatedError += "Determinacion de cuotaparte: CredxSus (creditos por subscripciones) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.creditoPorVentas":
                            translatedError += "Determinacion de cuotaparte: CredxVtasLiqNorm (creditos por ventas) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.creditoPorVentasOtros":
                            translatedError += "Determinacion de cuotaparte: CredxVtasOP (otros creditos por ventas) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.creditoOtros":
                            translatedError += "Determinacion de cuotaparte: CredOtros (otros creditos) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.activoOtros":
                            translatedError += "Determinacion de cuotaparte: ActOtros (otros activos) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.activoTotal":
                            translatedError += "Determinacion de cuotaparte: ActTotal (total activo) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.deudaPorRescates":
                            translatedError += "Determinacion de cuotaparte: DeuxRes (deudas por rescates) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.deudaPorCompras":
                            translatedError += "Determinacion de cuotaparte: DeuxCompLiqNorm (deudas por compras) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.deudaPorComprasOtros":
                            translatedError += "Determinacion de cuotaparte: DeuxCompOP (otras deudas por compras) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.deudaOtras":
                            translatedError += "Determinacion de cuotaparte: DeuOtras (otras deudas) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.provisiones":
                            translatedError += "Determinacion de cuotaparte: Prov (provisiones) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.pasivoOtros":
                            translatedError += "Determinacion de cuotaparte: PasOtros (otros pasivos) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.pasivoTotal":
                            translatedError += "Determinacion de cuotaparte: PasTotal (pasivo total) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "determinacionCuotaparte.patrimonioNetoTotal":
                            translatedError += "Determinacion de cuotaparte: PNTotal (patrimonio neto total) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "tipoCambio.tipo":
                            translatedError += "Cambio de " + element + ": TCambioCod (tipo de cambio) tiene un formato incorrecto. Debe ser uno de los siguientes valores: 'v' (venta), 'c' (compra).";
                            break;
                        case "tipoCambio.monedaOrigenTipoCodigo":
                            translatedError += "Cambio de " + element + ": MonOTCod (tipo de código de la moneda origen) tiene un formato incorrecto. Debe ser uno de los siguientes valores: 'c' (cafci), 'p' (propio).";
                            break;
                        case "tipoCambio.monedaOrigenCodigo":
                            translatedError += "Cambio de " + element + ": MonOCod (código de la moneda origen) tiene un formato incorrecto.";
                            break;
                        case "tipoCambio.monedaOrigenNombre":
                            translatedError += "Cambio de " + element + ": MonONom (nombre de la moneda origen) tiene un formato incorrecto.";
                            break;
                        case "tipoCambio.monedaOrigenCantidad":
                            translatedError += "Cambio de " + element + ": MonOCant (cantidad de la moneda origen) tiene un formato incorrecto. Debe ser un número con 8 enteros y 2 decimales.";
                            break;
                        case "tipoCambio.monedaDestinoTipoCodigo":
                            translatedError += "Cambio de " + element + ": MonDTCod (tipo de código de la moneda destino) tiene un formato incorrecto. Debe ser uno de los siguientes valores: 'c' (cafci), 'p' (propio).";
                            break;
                        case "tipoCambio.monedaDestinoCodigo":
                            translatedError += "Cambio de " + element + ": MonDCod (código de la moneda destino) tiene un formato incorrecto.";
                            break;
                        case "tipoCambio.monedaDestinoNombre":
                            translatedError += "Cambio de " + element + ": MonDNom (nombre de la moneda destino) tiene un formato incorrecto.";
                            break;
                        case "tipoCambio.monedaDestinoCantidad":
                            translatedError += "Cambio de " + element + ": MonDCant (cantidad de la moneda destino) tiene un formato incorrecto. Debe ser un número con 8 enteros y 2 decimales.";
                            break;
                        case "carteraItem.orden":
                            translatedError += "Cartera de activos, activo de orden " + element + ": Nro (numero de orden) tiene un formato incorrecto. Debe ser un número entero.";
                            break;
                        case "carteraItem.tipoCodigoTickerActivo":
                            translatedError += "Cartera de activos, activo de orden " + element + ": ActTTic (tipo de código del activo) tiene un formato incorrecto. Debe tener un máximo de 2 caracteres.";
                            break;
                        case "carteraItem.codigoTickerActivo":
                            translatedError += "Cartera de activos, activo de orden " + element + ": ActTic (código del activo) tiene un formato incorrecto. No puede estar vacio.";
                            break;
                        case "carteraItem.activoNombreCorto":
                            translatedError += "Cartera de activos, activo de orden " + element + ": ActNom (nombre del activo) tiene un formato incorrecto.";
                            break;
                        case "carteraItem.emisoraNombre":
                            translatedError += "Cartera de activos, activo de orden " + element + ": EntENom (nombre de la entidad emisora) tiene un formato incorrecto.";
                            break;
                        case "carteraItem.organizadoraNombre":
                            translatedError += "Cartera de activos, activo de orden " + element + ": EntONom (nombre de la entidad organizadora) tiene un formato incorrecto. ";
                            break;
                        case "carteraItem.mercadoTipoCodigo":
                            translatedError += "Cartera de activos, activo de orden " + element + ": MerTCod (tipo de código del mercado) tiene un formato incorrecto. Debe ser uno de los siguientes valores: 'c' (cafci), 'p' (propio).";
                            break;
                        case "carteraItem.mercadoCodigo":
                            translatedError += "Cartera de activos, activo de orden " + element + ": MerCod (código del mercado) tiene un formato incorrecto.";
                            break;
                        case "carteraItem.mercadoNombre":
                            translatedError += "Cartera de activos, activo de orden " + element + ": MerNom (nombre del mercado) tiene un formato incorrecto.";
                            break;
                        case "carteraItem.monedaTipoCodigo":
                            translatedError += "Cartera de activos, activo de orden " + element + ": MonActTCod (tipo de código de la moneda) tiene un formato incorrecto. Debe ser uno de los siguientes valores: 'c' (cafci), 'p' (propio).";
                            break;
                        case "carteraItem.monedaCodigo":
                            translatedError += "Cartera de activos, activo de orden " + element + ": MonActCod (código de la moneda) tiene un formato incorrecto.";
                            break;
                        case "carteraItem.monedaNombre":
                            translatedError += "Cartera de activos, activo de orden " + element + ": MonActNom (nombre de la moneda) tiene un formato incorrecto.";
                            break;
                        case "carteraItem.cantidad":
                            translatedError += "Cartera de activos, activo de orden " + element + ": Cant (cantidad) tiene un formato incorrecto. Debe ser un número con 12 enteros y 8 decimales.";
                            break;
                        case "carteraItem.cotizacionCantidad":
                            translatedError += "Cartera de activos, activo de orden " + element + ": CantCot (cantidad de cotizacion) tiene un formato incorrecto. Debe ser un entero.";
                            break;
                        case "carteraItem.cotizacionPrecio":
                            translatedError += "Cartera de activos, activo de orden " + element + ": Pre (precio de cotizacion) tiene un formato incorrecto. Debe ser un número con 16 enteros y 4 decimales.";
                            break;
                        case "carteraItem.cotizacionMontoOrigen":
                            translatedError += "Cartera de activos, activo de orden " + element + ": MtoMAct (monto en moneda del activo) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "carteraItem.cotizacionTipoCambio":
                            translatedError += "Cartera de activos, activo de orden " + element + ": TCCod (tipo de cambio) tiene un formato incorrecto. Debe ser uno de los siguientes valores: 'C', 'V', 'N'.";
                            break;
                        case "carteraItem.cotizacionMontoFondo":
                            translatedError += "Cartera de activos, activo de orden " + element + ": MtoMFdo (monto en moneda del fondo) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "carteraItem.claseActivoNombre":
                            translatedError += "Cartera de activos, activo de orden " + element + ": Cl (clase del activo) tiene un formato incorrecto.";
                            break;
                        case "carteraItem.subclaseActivoNombre":
                            translatedError += "Cartera de activos, activo de orden " + element + ": SCl (subclase del activo) tiene un formato incorrecto.";
                            break;
                        case "carteraItem.fechaOrigen":
                            translatedError += "Cartera de activos, activo de orden " + element + ": FeOr (fecha origen) tiene un formato incorrecto. Debe ser una fecha.";
                            break;
                        case "carteraItem.fechaInicioDevengamiento":
                            translatedError += "Cartera de activos, activo de orden " + element + ": TDFDev (fecha inicio devengamiento) tiene un formato incorrecto. Debe ser una fecha.";
                            break;
                        case "carteraItem.tna":
                            translatedError += "Cartera de activos, activo de orden " + element + ": TNA tiene un formato incorrecto. Debe ser un número con 6 enteros y 4 decimales.";
                            break;
                        case "carteraItem.tipoTasa":
                            translatedError += "Cartera de activos, activo de orden " + element + ": TTasa (tipo de tasa) tiene un formato incorrecto. Debe ser uno de los siguientes valores: 'F', 'O'.";
                            break;
                        case "carteraItem.montoActual":
                            translatedError += "Cartera de activos, activo de orden " + element + ": MtoAc (monto actual) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "carteraItem.fechaVencimiento":
                            translatedError += "Cartera de activos, activo de orden " + element + ": FeVto (fecha vencimiento) tiene un formato incorrecto. Debe ser una fecha.";
                            break;
                        case "carteraItem.plazo":
                            translatedError += "Cartera de activos, activo de orden " + element + ": Plazo tiene un formato incorrecto. Debe ser un entero.";
                            break;
                        case "carteraItem.transferible":
                            translatedError += "Cartera de activos, activo de orden " + element + ": PFTra (transferible) tiene un formato incorrecto. Debe ser uno de los siguientes valores: 'S' (sí), 'N' (no).";
                            break;
                        case "carteraItem.precancelable":
                            translatedError += "Cartera de activos, activo de orden " + element + ": PFPre (precancelable) tiene un formato incorrecto. Debe ser uno de los siguientes valores: 'S' (sí), 'N' (no).";
                            break;
                        case "carteraItem.precancelableInmediato":
                            translatedError += "Cartera de activos, activo de orden " + element + ": PFPreInm (precancelable inmediato) tiene un formato incorrecto. Debe ser uno de los siguientes valores: 'S' (sí), 'N' (no).";
                            break;
                        case "carteraItem.fechaPrimerPrecancelacion":
                            translatedError += "Cartera de activos, activo de orden " + element + ": PFPreF (fecha de primer precancelacion) tiene un formato incorrecto. Debe ser una fecha.";
                            break;
                        case "carteraItem.afectaMargenLiquidez":
                            translatedError += "Cartera de activos, activo de orden " + element + ": ML (afecta margen liquidez) tiene un formato incorrecto. Debe ser uno de los siguientes valores: 'S' (sí), 'N' (no).";
                            break;
                        case "carteraItem.precioEjercicio":
                            translatedError += "Cartera de activos, activo de orden " + element + ": DOPEj (precio ejercicio) tiene un formato incorrecto. Debe ser un número con 16 enteros y 4 decimales.";
                            break;
                        case "carteraItem.fechaEjercicio":
                            translatedError += "Cartera de activos, activo de orden " + element + ": DOFEj (fecha ejercicio) tiene un formato incorrecto. Debe ser una fecha.";
                            break;
                        case "carteraItem.activoSubyacente":
                            translatedError += "Item de cartera de orden " + element + ": DOSub (activo subyacente) tiene un formato incorrecto. Debe ser un entero.";
                            break;
                        case "inversor.codigoSubtipo":
                            translatedError += "Inversores para subtipo " + element + ": SubTipoCod (codigo de subtipo) tiene un formato incorrecto. Debe tener un máximo de 10 caracteres.";
                            break;
                        case "inversor.cantidad":
                            translatedError += "Inversores para subtipo " + element + ": CantInv (cantidad de inversores) tiene un formato incorrecto. Debe ser un entero.";
                            break;
                        case "inversor.monto":
                            translatedError += "Inversores para subtipo " + element + ": Monto tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "tenedor.tipoCodigoPais":
                            translatedError += "Residencias de inversores para " + element + ": PaisTCod (tipo de código del país) tiene un formato incorrecto. Debe tener un máximo de 1 caracter.";
                            break;
                        case "tenedor.codigoPais":
                            translatedError += "Residencias de inversores para " + element + ": PaisCod (código del país) tiene un formato incorrecto. Debe tener un máximo de 10 caracteres.";
                            break;
                        case "tenedor.nombrePais":
                            translatedError += "Residencias de inversores para " + element + ": PaisNom (nombre del país) tiene un formato incorrecto. Debe tener un máximo de 30 caracteres.";
                            break;
                        case "tenedor.cantidad":
                            translatedError += "Residencias de inversores para " + element + ": CCPPais (cantidad de cuotapertes) tiene un formato incorrecto. Debe ser un entero.";
                            break;
                        case "tenedor.monto":
                            translatedError += "Residencias de inversores para " + element + ": MontoPais (monto del país) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "mhc.claseId":
                            translatedError += "Honorarios y Comisiones para clase " + element + ": ClaseId (id de la clase) tiene un formato incorrecto. Debe ser un entero de 4 caracteres.";
                            break;
                        case "mhc.nombreClase":
                            translatedError += "Honorarios y Comisiones para clase " + element + ": ClaseNom (nombre de la clase) tiene un formato incorrecto. Debe tener un máximo de 50 caracteres.";
                            break;
                        case "mhc.minimoInversion":
                            translatedError += "Honorarios y Comisiones para clase " + element + ": InvMinima (mínimo de inversión) tiene un formato incorrecto. Debe ser un número con 18 enteros y 2 decimales.";
                            break;
                        case "mhc.honorariosAdministracionGerente":
                            translatedError += "Honorarios y Comisiones para clase " + element + ": HAdmAA (honorarios administrativos de la gerente) tiene un formato incorrecto. Debe ser un número con 4 enteros y 4 decimales.";
                            break;
                        case "mhc.honorariosAdministracionDepositaria":
                            translatedError += "Honorarios y Comisiones para clase " + element + ": HAdmAC (honorarios administrativos de la depositaria) tiene un formato incorrecto. Debe ser un número con 4 enteros y 4 decimales.";
                            break;
                        case "mhc.honorariosExito":
                            translatedError += "Honorarios y Comisiones para clase " + element + ": HExito (honorarios éxito) tiene un formato incorrecto. Debe tener un máximo de 1 caracter.";
                            break;
                        case "mhc.gastosGestion":
                            translatedError += "Honorarios y Comisiones para clase " + element + ": GOrdG (gastos ordinarios de gestión) tiene un formato incorrecto. Debe ser un número con 4 enteros y 4 decimales.";
                            break;
                        case "mhc.comisionIngreso":
                            translatedError += "Honorarios y Comisiones para clase " + element + ": CIngreso (comisión de ingreso) tiene un formato incorrecto. Debe ser un número con 4 enteros y 4 decimales.";
                            break;
                        case "mhc.comisionRescate":
                            translatedError += "Honorarios y Comisiones para clase " + element + ": CRescate (comisión de rescate) tiene un formato incorrecto. Debe ser un número con 4 enteros y 4 decimales.";
                            break;
                        case "mhc.comisionTransferencia":
                            translatedError += "Honorarios y Comisiones para clase " + element + ": CTransf (comisión de transferencia) tiene un formato incorrecto. Debe ser un número con 4 enteros y 4 decimales.";
                            break;
                        case "distribucion.claseId":
                            translatedError += "Distribuciones para clase " + element + ": ClaseId (id de la clase) tiene un formato incorrecto. Debe ser un entero de 4 caracteres.";
                            break;
                        case "distribucion.claseNombre":
                            translatedError += "Distribuciones para clase " + element + ": ClaseNom (nombre de la clase) tiene un formato incorrecto. Debe tener un maximo de 50 caracteres.";
                            break;
                        case "distribucion.fechaInicio":
                            translatedError += "Distribuciones para clase " + element + ": FechaIniPe (fecha de inicio del periodo) tiene un formato incorrecto. Debe ser una fecha.";
                            break;
                        case "distribucion.valorInicio":
                            translatedError += "Distribuciones para clase " + element + ": VCPUnitIni (valor de cuotaparte unitario en el inicio del periodo) tiene un formato incorrecto. Debe ser un número con 17 enteros y 7 decimales.";
                            break;
                        case "distribucion.fechaCierre":
                            translatedError += "Distribuciones para clase " + element + ": FechaCiePe (fecha de cierre del periodo) tiene un formato incorrecto. Debe ser una fecha.";
                            break;
                        case "distribucion.valorCierre":
                            translatedError += "Distribuciones para clase " + element + ": VCPUnitCie (valor de cuotaparte unitario en el cierre del periodo) tiene un formato incorrecto. Debe ser un número con 17 enteros y 7 decimales.";
                            break;
                        case "distribucion.utilidad":
                            translatedError += "Distribuciones para clase " + element + ": UtilxCP (utilidad por cuotaparte) tiene un formato incorrecto. Debe ser un número con 17 enteros y 7 decimales.";
                            break;
                        case "distribucion.totalDistribuido":
                            translatedError += "Distribuciones para clase " + element + ": MontoTotDist (monto total distribuido) tiene un formato incorrecto. Debe ser un número con 15 enteros y 2 decimales.";
                            break;
                        case "distribucion.porcentajeDistribucion":
                            translatedError += "Distribuciones para clase " + element + ": PorcDist (porcentaje de distribucion) tiene un formato incorrecto. Debe ser un número con 3 enteros y 2 decimales.";
                            break;
                        case "distribucion.fechaCorte":
                            translatedError += "Distribuciones para clase " + element + ": FechaCorte (fecha de corte) tiene un formato incorrecto. Debe ser una fecha.";
                            break;
                        case "distribucion.fechaPago":
                            translatedError += "Distribuciones para clase " + element + ": FechaPago (fecha de pago) tiene un formato incorrecto. Debe ser una fecha.";
                            break;
                        default:
                            break;
                    }
                    break;
                case "GT0066":
                    translatedError = "GT0066: El campo tipo de interfaz es incorrecto.";
                    break;
                case "GT0067":
                    translatedError = "GT0067: Hubo un error en nuestros servidores. Intente nuevamente mas tarde.";
                    break;
                case "GT0068":
                    translatedError = "GT0068: El archivo fue firmado con una firma incorrecta.";
                    break;
                case "GT0069":
                    translatedError = "GT0069: El usuario que solicitó acuse de recibo no existe.";
                    break;
                /* Interfaz Diaria Errors */
                case "VT0009":
                    translatedError = "VT0009: La estrutura del archivo XML es incorrecta. Archivo: "+ fileName;
                    break;
                case "VT0010":
                    translatedError = "VT0010: Existen tags incorrectos incluídos en el archivo XML. Archivo: "+ fileName;
                    break;
                /* Interfaz Semanal Errors */
                case "CT0028":
                    translatedError = "CT0028: La estrutura del archivo XML es incorrecta. Archivo: "+ fileName;
                    break;
                case "CT0029":
                    translatedError = "CT0029: Existen tags incorrectos incluídos en el archivo XML. Archivo: "+ fileName;
                    break;
                case "CT0031":
                    translatedError = "CT0031: El formato de la información recibida mediante archivo XML es incorrecto. Archivo: "+ fileName;
                    break;
                case "CT0032":
                    translatedError = "CT0032: La información requerida para el activo no está completa. Orden del item de cartera: "+ extraData;
                    break;
                case "CT0033":
                    translatedError = "CT0033: El código de \"tipo de cambio\" no se encuentra dado de alta en el \"Maestro de Activos\". Orden del item de cartera: "+ extraData;
                    break;
                case "CT0034":
                    translatedError = "CT0034: El código de \"activo\" no se encuentra dado de alta en el \"Maestro de Activos\". Orden del item de cartera: "+ extraData;
                    break;
                case "CT0035":
                    translatedError = "CT0035: El código de \"mercado\" no se encuentra dado de alta en el \"Maestro de Activos\". Orden del item de cartera: "+ extraData;
                    break;
                case "CT0036":
                    translatedError = "CT0036: El código de \"moneda\" no se encuentra dado de alta en el \"Maestro de Activos\". Orden del item de cartera: "+ extraData;
                    break;
                case "CT0037":
                    translatedError = "CT0037: El código de \"tipo de tasa\" no se encuentra dado de alta en el \"Maestro de Activos\". Orden del item de cartera: "+ extraData;
                    break;
                case "CT0038":
                    translatedError = "CT0038: El monto en la moneda del Fondo no coincide con el tipo de cambio informado. Orden del item de cartera: "+ extraData;
                    break;
                case "CT0039":
                    translatedError = "CT0039: No es posible informar valores nulos. Orden del item de cartera: "+ extraData;
                    break;
                case "CT0040":
                    translatedError = "CT0040: No es posible informar valores nulos. Orden del item de cartera: "+ extraData;
                    break;
                case "CT0041":
                    translatedError = "CT0041: No es posible informar valores nulos. Orden del item de cartera: "+ extraData;
                    break;
                case "CT0042":
                    translatedError = "CT0042: No es posible informar valores nulos. Orden del item de cartera: "+ extraData;
                    break;
                case "CT0043":
                    translatedError = "CT0043: No es posible informar valores nulos. Orden del item de cartera: "+ extraData;
                    break;
                case "CT0044":
                    translatedError = "CT0044: No es posible informar valores nulos. Orden del item de cartera: "+ extraData;
                    break;
                case "CT0045":
                    translatedError = "CT0045: No es posible informar valores nulos. Orden del item de cartera: "+ extraData;
                    break;
                case "CT0046":
                    translatedError = "CT0046: No es posible informar valores nulos. Orden del item de cartera: "+ extraData;
                    break;
                case "CT0047":
                    translatedError = "CT0047: No se ha recibido información de \"duration\". ID del fondo: "+ extraData;
                    break;
                case "CT0048":
                    translatedError = "CT0048: El control de \"Activo Total - Pasivo Total = PN\" no es correcto.";
                    break;
                case "CT0049":
                    translatedError = "CT0049: La suma de los items que integran el \"Activo Total\" no coincide con lo informado.";
                    break;
                case "CT0050":
                    translatedError = "CT0050: La suma de los items que integran el \"Pasivo Total\" no coincide con lo informado.";
                    break;
                case "CT0070":
                    translatedError = "CT0070: La información que se está enviando ya fue, recibida, procesada e informada a la CNV en anteriores despachos.";
                    break;
                case "CT0071":
                    translatedError = "CT0071: El monto en la moneda del activo informado en el ítem de cartera " + extraData + " no se corresponde con los valores de cantidad, cantidad de cotización y precio provistos.";
                    break;
                case "CT0077":
                    translatedError = "CT0077: El código de \"moneda\" no se encuentra dado de alta en el \"Maestro de Activos\". " + extraData + "(en tipos de cambio).";
                    break;
                /* Interfaz Mensual Errors */
                case "MT0051":
                    translatedError = "MT0051: La estrutura del archivo XML es incorrecta. Archivo: "+ fileName;
                    break;
                case "MT0053":
                    translatedError = "MT0053: Existen tags incorrectos incluídos en el archivo XML. Archivo: "+ fileName;
                    break;
                case "MT0055":
                    translatedError = "MT0055: El formato de la información recibida mediante archivo XML es incorrecto. Archivo: "+ fileName;
                    break;
                case "MT0056":
                    translatedError = "MT0056: El código de país no existe en la base de datos. Codigo de País: "+ extraData;
                    break;
                case "MT0057":
                    translatedError = "MT0057: El código de subtipo de inversor no existe en la base de datos. Codigo de Subtipo: "+ extraData;
                    break;
                case "MT0072":
                    translatedError = "MT0072: Hay mas de un inversor con codigo TOTAL.";
                    break;
                case "MT0073":
                    translatedError = "MT0073: No se informo el inversor con codigo TOTAL.";
                    break;
                case "MT0074":
                    translatedError = "MT0074: El inversor de la posicion " + extraData + " no indica cantidad.";
                    break;
                case "MT0075":
                    translatedError = "MT0075: El inversor de la posicion " + extraData + " no indica monto.";
                    break;
                case "UT0076":
                    translatedError = "UT0076: La estructura del archivo XML es incorrecta. Archivo: " + fileName;
                    break;
                case "UT0077":
                    translatedError = "UT0077: El formato de la información recibida mediante archivo XML es incorrecto. Archivo: " + fileName;
                    break;
                case "UT0078":
                    translatedError = "UT0078: La información recibida no tiene datos de valores diarios, cartera, ni inversores.";
                    break;
                // End of new error codes
                /* Interfaz Diaria errors */
                case "past-date":
                    translatedError = "No se pueden enviar interfaces para esa fecha de datos.";
                    break;
                case "inexistent-gerente":
                    translatedError = "La interfaz no informa la sociedad gerente, o bien ésta no existe.";
                    break;
                case "invalid-gerente-user":
                    translatedError = "Su usuario no corresponde a la Sociedad Gerente de la información que desea enviar.";
                    break;
                case "invalid-tipo-entidad":
                    translatedError = "La sociedad informada no es una sociedad gerente válida.";
                    break;
                case "inexistent-fondo":
                    translatedError = "La interfaz no informa el fondo, o bien éste no existe.";
                    break;
                case "public-key-inexistence":
                case "invalid-sign":
                    translatedError = "El archivo fue firmado con una firma incorrecta.";
                    break;
                case "invalid-fondo-gerente":
                case "invalid-fondo":
                    translatedError = "Una clase de fondo no pertenece a la sociedad gerente del usuario actual.";
                    break;
                case "inexistent-cotizaciones":
                    translatedError = "La interfaz no informa las cotizaciones.";
                    break;
                case "inexistent-cartera":
                    translatedError = "La interfaz no informa la cartera.";
                    break;
                case "inexistent-moneda":
                    if (String.IsNullOrEmpty(extraData))
                    {
                        translatedError = "Una de las monedas informadas no existe";
                    }
                    else
                    {
                        translatedError = "La moneda " + extraData + " no existe.";
                    }
                    break;
                case "inexistent-mercado":
                    if (String.IsNullOrEmpty(extraData))
                    {
                        translatedError = "Uno de los mercados informados no existe";
                    }
                    else
                    {
                        translatedError = "El mercado " + extraData + " no existe.";
                    }
                    break;
                case "inexistent-moneda-origen":
                    if (String.IsNullOrEmpty(extraData))
                    {
                        translatedError = "Una de las monedas de origen informadas no existe";
                    }
                    else
                    {
                        translatedError = "La moneda de origen " + extraData + " no existe.";
                    }
                    break;
                case "inexistent-moneda-destino":
                    if (String.IsNullOrEmpty(extraData))
                    {
                        translatedError = "Una de las monedas de destino informadas no existe";
                    }
                    else
                    {
                        translatedError = "La moneda de destino " + extraData + " no existe.";
                    }
                    break;
                case "invalid-missing-vdiarios":
                    translatedError = "Faltan informar algunas clases de fondo de la sociedad gerente.";
                    break;
                case "connection":
                    translatedError = "Hubo un error de conexión. Asegurese que tiene internet y reinicie la aplicacion.";
                    break;
                /* Login errors */
                case "internal-bad":
                case "internal-forbidden":
                    if (fromLogin)
                    {
                        translatedError = "Combinacion incorrecta de usuario y contrase\u00F1a. Intente con otra.";
                    }
                    else
                    {
                        translatedError = "Hubo un error en nuestros servidores. Intente nuevamente mas tarde.";
                    }
                    break;
                case "invalid_grant":
                    translatedError = "Combinacion incorrecta de usuario y contrase\u00F1a. Intente con otra.";
                    break;
                case "pass-expired":
                    translatedError = "Su contrase\u00F1a ha expirado. Por favor configure una nueva desde el cms.";
                    break;
                case "not-logged":
                case "unauthorized":
                    translatedError = "La sesion actual acaba de expirar. Por favor vuelva a loguearse.";
                    break;
                case "invalid-object":
                    translatedError = "El campo tipo de interfaz es incorrecto.";
                    break;
                case "inexistent-activo":
                    if (String.IsNullOrEmpty(extraData))
                    {
                        translatedError = "Al menos uno de los activos no existe.";
                    } else
                    {
                        translatedError = "El activo "+ extraData +" no existe.";
                    }
                    break;
                case "invalid-cartera-item":
                    translatedError = "Un item de cartera es invalido. Verifica que estén completados los campos obligatorios.";
                    break;
                case "inexistent-tenedores":
                    translatedError = "La interfaz no informa los tenedores.";
                    break;
                case "invalid-tenedor":
                    translatedError = "Uno de los tenedores es inválido.";
                    break;
                case "inexistent-pais":
                    translatedError = "Uno de los paises listados no existe.";
                    break;
                case "inexistent-calificaciones":
                    translatedError = "La interfaz no informa las calificaciones.";
                    break;
                case "invalid-calificacion":
                    translatedError = "Una de las calificaciones es inválido.";
                    break;
                case "inexistent-calificadora":
                    translatedError = "Una de las calificadoras listadas no existe.";
                    break;
                case "inexistent-mhcs":
                    translatedError = "La interfaz no informa los mhcs";
                    break;
                case "invalid-mhc":
                    translatedError = "Uno de los mhcs es inválido.";
                    break;
                case "inexistent-clase":
                    translatedError = "Una de las clases de fondo listadas no existe";
                    break;
                case "inexistent-interfaz":
                    translatedError = "La interfaz para la cual se solicitó acuse de recibo no existe.";
                    break;
                case "inexistent-user":
                    translatedError = "El usuario que solicitó acuse de recibo no existe.";
                    break;
                case "inexistent-file":
                    translatedError = "El archivo que desea enviar no existe, por favor intente pulsando 'recuperar' nuevamente para actualizar los contenidos de la carpeta.";
                    break;
                case "invalid-interfaz":
                    translatedError = "Hubo un error en el formato de la interfaz. Consulte con quien corresponda.";
                    break;
                default:
                    Console.WriteLine("Error: {0}", error);
                    translatedError = "GT0067: Hubo un error en nuestros servidores. Intente nuevamente mas tarde.";
                    break;
            }
            return translatedError;
        }

        /// <summary>
        /// Translates the api warning for humans to read it
        /// </summary>
        /// <param name="error">Warning from the api</param>
        /// <returns>Human readable warning</returns>
        public static string translateWarning(dynamic warning, string fileName = "")
        {
            string translatedError = "";
            string extraData = "";
            try {
                extraData = (string) warning.data;
            }
            catch (Exception e)
            {
                extraData = "";
            }
            string porcentajeTolerancia = "";
            try
            {
                porcentajeTolerancia = (string)warning.porcentajeTolerancia;
            }
            catch (Exception e)
            {
                porcentajeTolerancia = "";
            }

            switch ((string)warning.warn)
            {
                // v5.1 error codes
                case "T.0001":
                    translatedError = "T.0001: No se han encontrado archivos de interfaz.";
                    break;
                case "T.0002":
                    translatedError = "T.0002: La interfaz posee caracteres no válidos: "+ fileName;
                    break;
                case "T.0003":
                    translatedError = "T.0003: La estructura del archivo XML es incorrecta: "+ fileName;
                    break;
                case "T.0004":
                    translatedError = "T.0004: Existen tags incorrectos incluidos en el archivo XML: "+ fileName;
                    break;
                case "T.0005":
                    translatedError = "T.0005: El orden de los tags del archivo XML es incorrecto: "+ fileName;
                    break;
                case "T.0006":
                    translatedError = "T.0006: El formato de la información recibida es incorrecto: "+ extraData + " " + fileName;
                    break;
                case "T.0007":
                    translatedError = "T.0007: La Clase de Fondos no correspode a la Sociedad Gerente "+ extraData;
                    break;
                case "T.0008":
                    translatedError = "T.0008: El Fondo no corresponde a la Sociedad Gerente "+ extraData;
                    break;
                case "T.0009":
                    translatedError = "T.0009: El usuario ingresado no tiene relación con la Sociedad Gerente.";
                    break;
                case "T.0010":
                    translatedError = "T.0010: El valor \"fecha datos\" no es coincidente con la fecha de la interfaz.";
                    break;
                case "T.0011":
                    translatedError = "T.0011: El valor \"fecha datos\" no es válido.";
                    break;
                case "T.0012":
                    translatedError = "T.0012: El archivo fue firmado con una firma incorrecta.";
                    break;
                case "T.0013":
                    translatedError = "T.0013: ﻿Hubo un problema en las comunicaciones. Por favor reintente o comuníquese con la Cámara.";
                    break;
                case "T.0029":
                    translatedError = "T.0029: No se ha recibido información de Rendimiento Impositivo Diario para ID de la Clase: "+ extraData;
                    break;
                case "T.0030":
                    translatedError = "T.0030: La diferencia entre la suma del \"PN de las Clases\" y el \"PN Total del Fondo\" en la moneda del Fondo, excede el límite fijado.";
                    break;
                case "T.0031":
                    translatedError = "T.0031: El activo informado en el item número "+ extraData +" no se encuentra dado de alta en el \"Maestro de Activos\".";
                    break;
                case "T.0032":
                    translatedError = "T.0032: El dato requerido <"+extraData+"/> no se ha informado o es incorrecto.";
                    break;
                case "T.0033":
                    translatedError = "T.0033: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0034":
                    translatedError = "T.0034: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0035":
                    translatedError = "T.0035: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0036":
                    translatedError = "T.0036: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0037":
                    translatedError = "T.0037: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0038":
                    translatedError = "T.0038: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0039":
                    translatedError = "T.0039: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0040":
                    translatedError = "T.0040: No es posible informar valores nulos "+ extraData;
                    break;
                case "T.0041":
                    translatedError = "T.0041: No se ha recibido información de \"duration\" "+ extraData;
                    break;
                case "T.0042":
                    translatedError = "T.0042: El control de \"Activo Total - Pasivo Total = PN\" no es correto. ";
                    break;
                case "T.0043":
                    translatedError = "T.0043: La suma de los items que integran el \"Activo Total\" no coincide con lo informado. ";
                    break;
                case "T.0044":
                    translatedError = "T.0044: La suma de los items que integran el \"Pasivo Total\" no coincide con lo informado.";
                    break;
                case "T.0045":
                    translatedError = "T.0045: La información que se está enviando ya fue, recibida, procesada e informada a la CNV en anteriores despachos.";
                    break;
                case "T.0046":
                    translatedError = "T.0046: El monto en la moneda del activo informado en el ítem de cartera ("+ extraData +") no se corresponde con los valores de cantidad, cantidad de cotización y precio provistos. ";
                    break;
                case "T.0048":
                    translatedError = "T.0048: El activo informado en el item número "+ extraData +" no posee o es incorreta la información sobre \"Código Impositivo\". ";
                    break;
                case "T.0049":
                    translatedError = "T.0049: La cartera informada del Fondo "+ extraData +" no posee información sobre \"Cumplimiento de Activo Subyacente Principal\". ";
                    break;
                case "T.0050":
                    translatedError = "T.0050: El activo informado en el item número "+ extraData +"  no posee información sobre \"País Emisor\". ";
                    break;
                case "T.0051":
                    translatedError = "T.0051: El activo informado en el item número "+ extraData +"  no posee información sobre \"País Emisor\". ";
                    break;
                case "T.0052":
                    translatedError = "T.0052: El activo informado en el item número "+ extraData +" no posee información sobre \"País Negociado\". ";
                    break;
                case "T.0053":
                    translatedError = "T.0052: El activo informado en el item número "+ extraData +" no posee información sobre \"País Negociado\". ";
                    break;
                case "T.0143":
                    translatedError = "T.0143: Hay mas de un inversor con codigo TOTAL. ";
                    break;
                case "T.0144":
                    translatedError = "T.0144: No se informo el inversor con codigo TOTAL. ";
                    break;
                case "T.0145":
                    translatedError = "T.0145: El inversor de la posicion "+ extraData +" no indica cantidad. ";
                    break;
                case "T.0146":
                    translatedError = "T.0146: El inversor de la posicion "+ extraData +" no indica monto. ";
                    break;
                case "T.0147":
                    translatedError = "T.0147: La suma de los montos totales por tipo de inversor no coincide con el total general informado "+ extraData;
                    break;
                case "T.0148":
                    translatedError = "T.0148: La suma de las cantidades totales por tipo de inversor no coincide con el total general informado "+ extraData;
                    break;
                case "T.0149":
                    translatedError = "T.0149: La diferencia entre la cantidad total de personas físicas actual y anterior, excede el límite fijado "+ extraData;
                    break;
                case "T.0150":
                    translatedError = "T.0150: La diferencia entre la cantidad total de personas jurídicas actual y anterior, excede el límite fijado "+ extraData;
                    break;
                case "T.0151":
                    translatedError = "T.0151: La diferencia entre el monto total de personas físicas actual y anterior, excede el límite fijado "+ extraData;
                    break;
                case "T.0152":
                    translatedError = "T.0152: La diferencia entre el monto total de personas jurídicas actual y anterior, excede el límite fijado "+ extraData;
                    break;
                case "T.0153":
                    translatedError = "T.0153: No se ha recibido información para la siguiente Clase / Fondo. ID de la Clase: "+ extraData;
                    break;
                // v5.1 warning codes
                case "A.0014":
                    translatedError = "A.0014: El control de \"VCP * CCP = PN\", no es coincidente "+ extraData;
                    break;
                case "A.0015":
                    translatedError = "A.0015: La diferencia entre \"PN actual y PN anterior\", excede el límite fijado "+ extraData;
                    break;
                case "A.0016":
                    translatedError = "A.0016: La diferencia entre \"CCP actual y CCP anterior\", excede el límite fijado "+ extraData;
                    break;
                case "A.0017":
                    translatedError = "A.0017: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0018":
                    translatedError = "A.0018: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0019":
                    translatedError = "A.0019: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0020":
                    translatedError = "A.0020: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0021":
                    translatedError = "A.0021: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0022":
                    translatedError = "A.0022: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0023":
                    translatedError = "A.0023: La variación diaria de la Clase / Fondo, excede el límite fijado "+ extraData;
                    break;
                case "A.0024":
                    translatedError = "A.0024: El flujo diario de cantidad de cuotaparte no coincide con lo informado anteriormente. ";
                    break;
                case "A.0025":
                    translatedError = "A.0025: Se ha informado valor nulo para el PN "+ extraData;
                    break;
                case "A.0026":
                    translatedError = "A.0026: Se ha informado valor nulo para el VCP "+ extraData;
                    break;
                case "A.0027":
                    translatedError = "A.0027: Se ha informado valor nulo para el CCP "+ extraData;
                    break;
                case "A.0028":
                    translatedError = "A.0028: No se ha recibido información para la siguiente Clase / Fondo. ID de la Clase: "+ extraData;
                    break;
                case "A.0047":
                    translatedError = "A.0047: El valor \"fecha de vencimiento\" es menor que el valor \"fecha de datos\" "+ extraData;
                    break;
                case "A.0054":
                    translatedError = "A.0054: El activo que intenta dar de alta ya existe en el \"Maestro de Activos\": "+ extraData +". La solicitud de alta de este activo será ignorada.";
                    break;
                case "A.0055":
                    translatedError = "A.0055: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0056":
                    translatedError = "A.0055: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0057":
                    translatedError = "A.0057: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0058":
                    translatedError = "A.0058: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0059":
                    translatedError = "A.0059: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0060":
                    translatedError = "A.0060: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0061":
                    translatedError = "A.0061: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0062":
                    translatedError = "A.0062: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0063":
                    translatedError = "A.0063: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0064":
                    translatedError = "A.0064: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0065":
                    translatedError = "A.0065: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0066":
                    translatedError = "A.0066: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0067":
                    translatedError = "A.0067: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0068":
                    translatedError = "A.0068: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0069":
                    translatedError = "A.0069: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0070":
                    translatedError = "A.0070: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0071":
                    translatedError = "A.0071: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0072":
                    translatedError = "A.0072: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0073":
                    translatedError = "A.0073: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0074":
                    translatedError = "A.0074: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0075":
                    translatedError = "A.0075: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0076":
                    translatedError = "A.0076: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0077":
                    translatedError = "A.0077: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0078":
                    translatedError = "A.0078: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0079":
                    translatedError = "A.0079: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0080":
                    translatedError = "A.0080: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0081":
                    translatedError = "A.0081: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0082":
                    translatedError = "A.0082: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0083":
                    translatedError = "A.0083: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0084":
                    translatedError = "A.0084: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0085":
                    translatedError = "A.0085: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0086":
                    translatedError = "A.0086: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0087":
                    translatedError = "A.0087: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0088":
                    translatedError = "A.0088: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0089":
                    translatedError = "A.0089: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0090":
                    translatedError = "A.0090: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0091":
                    translatedError = "A.0091: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0092":
                    translatedError = "A.0092: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0093":
                    translatedError = "A.0093: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0094":
                    translatedError = "A.0094: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0095":
                    translatedError = "A.0095: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0096":
                    translatedError = "A.0096: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0097":
                    translatedError = "A.0097: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0098":
                    translatedError = "A.0098: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0099":
                    translatedError = "A.0099: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0100":
                    translatedError = "A.0100: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0101":
                    translatedError = "A.0101: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0102":
                    translatedError = "A.0102: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0103":
                    translatedError = "A.0103: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0104":
                    translatedError = "A.0104: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0105":
                    translatedError = "A.0105: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0106":
                    translatedError = "A.0106: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0107":
                    translatedError = "A.0107: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0108":
                    translatedError = "A.0108: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0109":
                    translatedError = "A.0109: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0110":
                    translatedError = "A.0110: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0111":
                    translatedError = "A.0111: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0112":
                    translatedError = "A.0112: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0113":
                    translatedError = "A.0113: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0114":
                    translatedError = "A.0114: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0115":
                    translatedError = "A.0115: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0116":
                    translatedError = "A.0116: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0117":
                    translatedError = "A.0117: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0118":
                    translatedError = "A.0118: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0119":
                    translatedError = "A.0119: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0120":
                    translatedError = "A.0120: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0121":
                    translatedError = "A.0121: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0122":
                    translatedError = "A.0122: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0123":
                    translatedError = "A.0123: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0124":
                    translatedError = "A.0124: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0125":
                    translatedError = "A.0125: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0126":
                    translatedError = "A.0126: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0127":
                    translatedError = "A.0127: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0128":
                    translatedError = "A.0128: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0129":
                    translatedError = "A.0129: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0130":
                    translatedError = "A.0130: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0131":
                    translatedError = "A.0131: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0132":
                    translatedError = "A.0132: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0133":
                    translatedError = "A.0133: El dato requerido no se ha informado o es incorrecto:"+ extraData;
                    break;
                case "A.0134":
                    translatedError = "A.0134: El dato requerido no se ha informado o es incorrecto:"+ extraData;
                    break;
                case "A.0135":
                    translatedError = "A.0135: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0136":
                    translatedError = "A.0136: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0137":
                    translatedError = "A.0137: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0138":
                    translatedError = "A.0138: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0139":
                    translatedError = "A.0139: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0140":
                    translatedError = "A.0140: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0141":
                    translatedError = "A.0141: El dato requerido no se ha informado o es incorrecto: "+ extraData;
                    break;
                case "A.0142":
                    translatedError = "A.0142: El dato requerido no se ha informado: "+ extraData;
                    break;
                case "A.0154":
                    translatedError = "A.0154: El PN informado en \"Valores Diarios\", no coincide con el PN informado en el bloque \"Carteras Inversiones\". ";
                    break;
                case "A.0155":
                    translatedError = "A.0155: El PN informado en \"Valores Diarios\", no coincide con el PN informado en el bloque \"Inversores y Comisiones\". ";
                    break;
                // v5.2 error codes
                case "T.0154":
                    translatedError = "T.0154: El código de \"moneda\" no se encuentra dado de alta en el \"Maestro de Activos\" " + extraData;
                    break;
                case "T.0155":
                    translatedError = "T.0155: El código de \"mercado\" no se encuentra dado de alta en el \"Maestro de Activos\" " + extraData;
                    break;
                case "T.0165":
                    translatedError = "T.0165: La información recibida no tiene datos de \"valores diarios\", \"cartera de inversiones\" ni \"inversores y comisiones\"";
                    break;
                case "A.0159":
                    translatedError = "A.0159: El \"PN en la moneda del Fondo\" no coincide con el tipo de cambio informado en función del \"PN de la Clase\" " + extraData;
                    break;
                case "T.0158":
                    translatedError = "T.0158: El código de \"moneda\" no se encuentra dado de alta en el \"Maestro de Activos\" " + extraData;
                    break;
                case "T.0160":
                    translatedError = "T.0160: La información requerida para el activo no está completa " + extraData;
                    break;
                case "T.0161":
                    translatedError = "T.0161: El código de \"tipo de tasa\" no se encuentra dado de alta en el \"Maestro de Activos\" " + extraData;
                    break;
                case "T.0162":
                    translatedError = "T.0162: El código de \"tipo de cambio\" no se encuentra dado de alta en el \"Maestro de Activos\" " + extraData;
                    break;
                case "A.0163":
                    translatedError = "A.0163: El monto en la moneda del Fondo " + extraData;
                    break;
                case "A.0164":
                    translatedError = "A.0164: El código de \"tipo de cambio\" no se encuentra dado de alta en el \"Maestro de Activos\" " + extraData;
                    break;
                case "A.0156":
                    translatedError = "A.0156: El PN informado en \"Valores Diarios\", no coincide con el PN informado en el bloque \"Carteras Inversiones\".";
                    break;
                case "A.0157":
                    translatedError = "A.0157: El PN informado en \"Valores Diarios\", no coincide con el PN informado en el bloque \"Inversores y Comisiones\".";
                    break;
                // New warning codes
                /* Interfaz Diaria Warning */
                case "VA0013":
                    translatedError = "VA0013: El control de \"VCP * CCP = PN\", no es coincidente. ID de la Clase: " + extraData;
                    break;
                case "VA0014":
                    translatedError = "VA0014: La diferencia entre \"PN actual y PN anterior\", excede el límite fijado. ID de la Clase: "+ extraData;
                    break;
                case "VA0015":
                    translatedError = "VA0015: La diferencia entre \"CCP actual y CCP anterior\", excede el límite fijado. ID de la Clase: "+ extraData;
                    break;
                case "VA0016":
                    translatedError = "VA0016: La variación diaria del valor de cuotaparte, excede el límite fijado en ±" + porcentajeTolerancia + "%. ID de la Clase: " + extraData;
                    break;
                case "VA0017":
                    translatedError = "VA0017: La variación diaria del valor de cuotaparte, excede el límite fijado en ±" + porcentajeTolerancia + "%. ID de la Clase: " + extraData;
                    break;
                case "VA0018":
                    translatedError = "VA0018: La variación diaria del valor de cuotaparte, excede el límite fijado en ±" + porcentajeTolerancia + "%. ID de la Clase: " + extraData;
                    break;
                case "VA0019":
                    translatedError = "VA0019: La variación diaria del valor de cuotaparte, excede el límite fijado en ±" + porcentajeTolerancia + "%. ID de la Clase: " + extraData;
                    break;
                case "VA0020":
                    translatedError = "VA0020: La variación diaria del valor de cuotaparte, excede el límite fijado en ±" + porcentajeTolerancia + "%. ID de la Clase: " + extraData;
                    break;
                case "VA0021":
                    translatedError = "VA0021: La variación diaria del valor de cuotaparte, excede el límite fijado en ±" + porcentajeTolerancia + "%. ID de la Clase: " + extraData;
                    break;
                case "VA0022":
                    translatedError = "VA0022: La variación diaria del valor de cuotaparte, excede el límite fijado en ±" + porcentajeTolerancia + "%. ID de la Clase: " + extraData;
                    break;
                case "VA0023":
                    translatedError = "VA0023: El flujo diario de cantidad de cuotaparte no coincide con lo informado anteriormente. ID de la clase: "+ extraData;
                    break;
                case "VA0024":
                    translatedError = "VA0024: Se ha informado valor nulo para el PN. ID de la Clase: "+ extraData;
                    break;
                case "VA0025":
                    translatedError = "VA0025: Se ha informado valor nulo para el VCP. ID de la Clase: "+ extraData;
                    break;
                case "VA0026":
                    translatedError = "VA0026: Se ha informado valor nulo para el CCP. ID de la Clase: "+ extraData;
                    break;
                case "VA0027":
                    translatedError = "VA0027: No se ha recibido información para la siguiente Clase. ID de la Clase: "+ extraData;
                    break;
                case "VA0078":
                    translatedError = "VA0078: El \"PN en la moneda del Fondo\" no coincide con el tipo de cambio informado en función del \"PN de la Clase\". ID de la Clase: " + extraData;
                    break;
                /* Interfaz Semanal Warning */
                case "CA0051":
                    translatedError = "CA0051: El valor \"fecha de vencimiento\" es menor que el valor \"fecha de datos\". Orden del item de cartera: "+ extraData;
                    break;
                case "CT0038":
                    translatedError = "CT0038: El monto en la moneda del Fondo (MtoMFdo) no coincide con el tipo de cambio informado. Orden del item de cartera: " + extraData;
                    break;
                case "CT0071":
                    translatedError = "CT0071: El monto en la moneda del activo informado en el ítem de cartera " + extraData + " no se corresponde con los valores de cantidad, cantidad de cotización y precio provistos.";
                    break;
                case "CT0079":
                    translatedError = "CT0079: El monto en la moneda del Fondo (Mto AC) no coincide con el tipo de cambio informado. Orden del item de cartera: " + extraData;
                    break;
                /* Interfaz Mensual Warning */
                case "MA0058":
                    translatedError = "MA0058: La suma de los montos totales por tipo de inversor no coincide con el total general informado. ID del Fondo: "+ extraData;
                    break;
                case "MA0059":
                    translatedError = "MA0059: La suma de las cantidades totales por tipo de inversor no coincide con el total general informado. ID del Fondo: "+ extraData;
                    break;
                case "MA0060":
                    translatedError = "MA0060: La diferencia entre la cantidad total de personas físicas actual y anterior, excede el límite fijado. ID del Fondo: "+ extraData;
                    break;
                case "MA0061":
                    translatedError = "MA0061: La diferencia entre la cantidad total de personas jurídicas actual y anterior, excede el límite fijado. ID del Fondo: "+ extraData;
                    break;
                case "MA0062":
                    translatedError = "MA0062: La diferencia entre el monto total de personas físicas actual y anterior, excede el límite fijado. ID del Fondo: "+ extraData;
                    break;
                case "MA0063":
                    translatedError = "MA0063: La diferencia entre el monto total de personas jurídicas actual y anterior, excede el límite fijado. ID del Fondo: "+ extraData;
                    break;
                case "MA0076":
                    translatedError = "MA0076: No se ha recibido información para la siguiente Clase / Fondo. ID de la Clase: " + extraData;
                    break;
                /* Mix interfaz Warning */
                case "EA0064":
                    translatedError = "EA0064: El PN informado no coincide con el PN informado en la interfaz diaria.";
                    break;
                case "EA0065":
                    translatedError = "EA0065: El PN informado no coincide con el PN informado en la interfaz diaria.";
                    break;
                case "UA0079":
                    translatedError = "UA0079: Se movió el tag de Tipo de Cambios dentro de la información de carteras de forma temporal.";
                    break; 
                // End of new warning codes
                case "invalid-patrimonio-neto":
                    translatedError = "La clase " + extraData + " tiene un patrimonio que no corresponde con el valor y la cantidad de cuotapartes.";
                    break;
                case "invalid-cuotaparte-anterior":
                    translatedError = "La clase " + extraData + " tiene una cantidad de cuotapartes que no corresponde con sus rescates y subscripciones.";
                    break;
                case "invalid-cantidad-cuotapartes":
                    translatedError = "La clase " + extraData + " vario mucho su cantidad de cuotapartes.";
                    break;
                case "invalid-valor-cuotapartes":
                    translatedError = "La clase " + extraData + " vario mucho su valor de cuotapartes.";
                    break;
                case "inexistent-item":
                    translatedError = "Item de cartera inexistente.";
                    break;
                case "inexistent-cotizacion":
                    translatedError = "El campo de cotización para ítem de cartera " + extraData + " no existe.";
                    break;
                case "inexistent-or-invalid-cotizacion-tipo-cambio":
                    translatedError = "El campo de tipo de cambio para la cotización del ítem de cartera " + extraData + " no existe o posee un valor inválido.";
                    break;
                case "inexistent-or-zero-cantidad":
                    translatedError = "El campo de cantidad para ïtem de cartera " + extraData + " no existe o es cero.";
                    break;
                case "inexistent-or-zero-cotizacion-cantidad":
                    translatedError = "El campo de cantidad para la cotización del ïtem de cartera " + extraData + " no existe o es cero.";
                    break;
                case "inexistent-or-zero-cotizacion-precio":
                    translatedError = "El campo de precio para la cotización del ïtem de cartera " + extraData + " no existe o es cero.";
                    break;
                case "inexistent-or-zero-cotizacion-origen":
                    translatedError = "El campo de monto de origen para la cotización del ïtem de cartera " + extraData + " no existe o es cero.";
                    break;
                case "inexistent-or-zero-cotizacion-fondo":
                    translatedError = "El campo de monto de fondo para la cotización del ïtem de cartera " + extraData + " no existe o es cero.";
                    break;
                case "invalid-cotizacion-monto-origen":
                    translatedError = "El campo de monto de origen para la cotización del ítem de cartera " + extraData + " posee un valor inválido de acuerdo a los valores de cantidad, cantidad de cotización y precio porvistos.";
                    break;
                case "invalid-cotizacion-monto-fondo":
                    translatedError = "El campo de monto de fondo para la cotización del ítem de cartera " + extraData + " posee un valor inválido de acuerdo a los valores de monto de origen y tasa de conversión provistos.";
                    break;
                case "missing-clase-fondo":
                    translatedError = "Al menos una de las clases de fondo pertenecientes a la sociedad gerente es está informada.";
                    break;
                case "mhc-missing-clase-id":
                case "mhc-missing-clase":
                    translatedError = "Uno de los mhc informados no posee información sobre su clase, o ésta se encuentra incompleta.";
                    break;
                case "inversor-missing-codigo-subtipo":
                    translatedError = "El inversor de la posicion " + extraData + " no indica codigo de subtipo";
                    break;
                case "multiple-inversor-total":
                    translatedError = "Hay mas de un inversor con codigo TOTAL";
                    break;
                case "inversor-missing-cantidad":
                    translatedError = "El inversor de la posicion " + extraData + " no indica cantidad";
                    break;
                case "inversor-missing-monto":
                    translatedError = "El inversor de la posicion " + extraData + " no indica monto";
                    break;
                case "inversor-no-total":
                    translatedError = "No se informo el inversor con codigo TOTAL";
                    break;
                case "suma-cantidad-missmatch":
                    translatedError = "La suma de las cantidades no es igual a la indicada en el total";
                    break;
                case "suma-monto-missmatch":
                    translatedError = "La suma de los montos no es igual al indicado en el total";
                    break;
                default:
                    Console.WriteLine("Warning undefined: {0}", warning.warn);
                    translatedError = "Aviso indefinido";
                    break;
            }
            return translatedError;
        }

        /// <summary>
        ///     Make http get request to the server
        /// </summary>
        /// <param name="url">Url of the request</param>
        /// <returns></returns>
        private async static Task<string> getData(string url, bool withAuth = false)
        {
            AuthenticationHeaderValue auth = null;
            if (withAuth)
            {
                auth = await getAuthenticationHeader();
            }
            string responseString = null;
            string error = null;
            using (var client = new HttpClient())
            {
                if (auth != null)
                {
                    client.DefaultRequestHeaders.Authorization = auth;
                }
                try
                {
                    //Agregar custom headers para identificacion del lado del API
                    client.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(new ProductHeaderValue(Helper.ORIGEN)));
                    client.DefaultRequestHeaders.Add("Origin", Helper.ORIGEN);

                    var response = await client.GetAsync(url);

                    if (!response.IsSuccessStatusCode)
                    {
                        error = "internal";
                    }
                    else
                    {
                        responseString = await response.Content.ReadAsStringAsync();
                        if (response.Content.Headers.ContentType.MediaType == "application/json")
                        {
                            var json = JsonConvert.DeserializeObject<dynamic>(responseString);
                            if (json.error != null)
                            {
                                if (json.error is JArray)
                                {
                                    json = json.error.First;
                                }
                                error = json.error;
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    error = "connection";
                }
            }
            if (error != null)
            {
                Console.WriteLine(error);
                throw new Exception(error);
            }
            return responseString;
        }

        private async static Task<string> readErrorFromResponseString(HttpResponseMessage response)
        {
            string error = null;
            var responseString = await response.Content.ReadAsStringAsync();
            if (response.Content.Headers.ContentType.MediaType == "application/json")
            {
                var json = JsonConvert.DeserializeObject<dynamic>(responseString);
                if (json.error != null)
                {
                    Console.WriteLine("el error es: {0}", json.error);
                    Console.WriteLine("atributos del error: {0}", json.attrs);
                    if (json.error is JArray)
                    {
                        json = json.error.First;
                    }
                    error = json.error;
                    try
                    {
                        if (json.data != null)
                        {
                            error += "|" + json.data;
                        }
                    }
                    catch (Exception e)
                    {
                        // No nombre. No problem.
                        error = json.error;
                    }
                }
            }
            if(error != null)
            {
                throw new Exception(error);
            }
            return responseString;
        }

        /// <summary>
        ///     Make http post request to the server
        /// </summary>
        /// <param name="url">Url of the request</param>
        /// <param name="content">Content to post</param>
        /// <returns>Response string or throws exceptions on error</returns>
        private async static Task<string> postData(string url, HttpContent content, bool withAuth = false)
        {
            AuthenticationHeaderValue auth = null;
            if (withAuth)
            {
                auth = await getAuthenticationHeader();
            }
            string responseString = null;
            string error = null;
            using (var client = new HttpClient())
            {
                if (auth != null)
                {
                    client.DefaultRequestHeaders.Authorization = auth;
                }
                try
                {
                    //Agregar custom headers para identificacion del lado del API
                    client.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(new ProductHeaderValue(Helper.ORIGEN)));
                    client.DefaultRequestHeaders.Add("Origin", Helper.ORIGEN);

                    client.Timeout = TimeSpan.FromMinutes(5);
                    Console.WriteLine("url: {0}", url);
                    var response = await client.PostAsync(url, content);
                    if (!response.IsSuccessStatusCode)
                    {
                        if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
                        {
                            error = "internal-bad";
                        } else if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
                        {
                            error = "internal-forbidden";
                        } else
                        {
                            try
                            {
                                await readErrorFromResponseString(response);
                                error = "internal";
                            } catch(Exception e)
                            {
                                error = e.Message;
                            }
                        }
                        Console.WriteLine("Error Code: {0}", response.StatusCode);
                    } else
                    {
                        try
                        {
                            responseString = await readErrorFromResponseString(response);
                        } catch(Exception e)
                        {
                            error = e.Message;
                        }
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine("Error as das da asd as da : {0}", e);
                    error = "connection";
                }
            }
            if (error != null)
            {
                throw new Exception(error);
            }
            return responseString;
        }

        /// <summary>
        ///     Make http get request to the server
        /// </summary>
        /// <param name="url">Url of the request</param>
        /// <returns></returns>
        private async static Task<bool> downloadData(string url, bool withAuth = false)
        {
            AuthenticationHeaderValue auth = null;
            if (withAuth)
            {
                auth = await getAuthenticationHeader();
            }
            byte[] responseBytes = null;
            string error = null;
            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(5);
                if (auth != null)
                {
                    client.DefaultRequestHeaders.Authorization = auth;
                }
                try
                {
                    //Agregar custom headers para identificacion del lado del API
                    client.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(new ProductHeaderValue(Helper.ORIGEN)));
                    client.DefaultRequestHeaders.Add("Origin", Helper.ORIGEN);

                    var response = await client.GetAsync(url);
                    if (!response.IsSuccessStatusCode)
                    {
                        error = "internal";
                    }
                    else
                    {
                        if (response.Content.Headers.ContentType.MediaType == "application/force-download")
                        {
                            responseBytes = await response.Content.ReadAsByteArrayAsync();
                            Console.WriteLine(Properties.Settings.Default.xmlPath + "file.txt");
                            FileStream file = File.Create(Properties.Settings.Default.xmlPath + "/" + response.Content.Headers.ContentDisposition.FileName);
                            file.Write(responseBytes, 0, responseBytes.Length);
                            file.Close();
                        } else if (response.Content.Headers.ContentType.MediaType == "application/json")
                        {
                            String responseString = await response.Content.ReadAsStringAsync();
                            var json = JsonConvert.DeserializeObject<dynamic>(responseString);
                            if (json.error != null)
                            {
                                if (json.error is JArray)
                                {
                                    json = json.error.First;
                                }
                                error = json.error;
                            }
                        } else
                        {
                            //no tengo idea que recibi de respuesta.
                            error = "internal";
                        }

                    }
                }
                catch
                {
                    error = "connection";
                }
            }
            if (error != null)
            {
                Console.WriteLine(error);
                throw new Exception(error);
                return false;
            }
            return true;
        }

        public static async Task<dynamic> getSociedadGerente()
        {
            string responseString = await getData(baseUrl + "/user/profile", true);
            dynamic json = JsonConvert.DeserializeObject<dynamic>(responseString);
            return json.data.entidad;
        }

        /* Get the entidad and fondo models */
        public static async Task<Dictionary<string, string>> getSociedades(string ids)
        {
            string responseString = await getData(baseUrl + "/entidad?id="+ ids +"&limit=0");
            dynamic json = JsonConvert.DeserializeObject<dynamic>(responseString);
            Dictionary<string, string> namesById = new Dictionary<string, string>();
            foreach (dynamic entidad in json.data)
            {
                namesById.Add(Int64.Parse(entidad.id.ToString()).ToString("D4"), entidad.nombre.ToString());
            }
            return namesById;
        }

        public static async Task<Dictionary<string, dynamic>> getFondos(string sociedadGerenteId)
        {
            string responseString = await getData(baseUrl + "/fondo?sociedadGerenteId="+ sociedadGerenteId +"&limit=0");
            dynamic json = JsonConvert.DeserializeObject<dynamic>(responseString);
            Dictionary<string, dynamic> namesById = new Dictionary<string, dynamic>();
            List<string> sociedades = new List<string>();
            sociedades.Add(sociedadGerenteId);
            foreach (dynamic fondo in json.data)
            {
                namesById.Add(Int64.Parse(fondo.id.ToString()).ToString("D4"), fondo);
                if(fondo.sociedadDepositariaId != null && !sociedades.Contains(fondo.sociedadDepositariaId.ToString()))
                {
                    sociedades.Add(fondo.sociedadDepositariaId.ToString());
                }
            }
            namesById["sociedades"] = String.Join("|", sociedades);
            return namesById;
        }

        /* Complete info */
        public static void completeAndSendInterfazUnica(Interfaz interfaz)
        {
            try
            {
                interfaz.completingFile();
                interfacesAwaitingResponse.Add(interfaz.name, interfaz);
                completeInterfaz(interfaz);
            }
            catch (Exception err)
            {
                Console.WriteLine("err.Message 1: ", err.Message);
                interfaz.onError(err.Message);
            }
        }

        private static async void completeInterfaz(Interfaz interfaz){
            /* Get all file contents */
            String file = await Task.Run(() => {
                try
                {
                    StreamReader stream = new StreamReader(interfaz.path, Encoding.UTF8);
                    String toReturn = stream.ReadToEnd();
                    stream.Close();
                    return toReturn;
                } 
                catch (Exception e)
                {
                    return "inexistent-file";
                }
            });

            var values = new Dictionary<string, string>
                    {
                       { "file", file },
                       { "fileName", interfaz.name },
                       { "multipleErrors", "true" },
                       { "version", Helper.VERSION }
                    };

            var jsonString = JsonConvert.SerializeObject(values);
            try
            {
                if (file != "inexistent-file")
                {
                    socketHelper.send("completarUnica", jsonString);
                } else
                {
                    interfaz.onError("inexistent-file");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("err.Message 2: ", e.Message);
                interfaz.onError(e.Message);
            }
        }

        /* Send info */

        private static async void sendSignedFile(Interfaz interfaz)
        {
            if (csp == null)
            {
                Console.WriteLine("Primero elegi una clave para firmar");
                return;
            }
            /* Get all file contents */
            String file = await Task.Run(() => {
                StreamReader stream = new StreamReader(interfaz.path, Encoding.UTF8);
                String toReturn = stream.ReadToEnd();
                stream.Close();
                return toReturn;
            });
            /* Sign the file */
            String signedString = await Helper.signFile(file, csp);
            /* Send the file with the sign */
            var values = new Dictionary<string, string>
                    {
                       { "file", file },
                       { "sign", signedString },
                       { "fileName", interfaz.name },
                       { "multipleErrors", "true" },
                       { "version", Helper.VERSION }
                    };

            var jsonString = JsonConvert.SerializeObject(values);
            try
            {
                socketHelper.send("guardarUnica", jsonString);
                interfaz.sendingFile();
            }
            catch (Exception e)
            {
                Console.WriteLine("err.Message 2: ", e.Message);
                interfaz.onError(e.Message);
            }
        }

        /* Get acuse de recibo */
        public static async void getAcuseRecibo(Interfaz interfaz)
        {

            try
            {
                interfaz.gettingRecibo();
                string tipo = "";
                switch (interfaz.tipo)
                {
                    case "Unica":
                        await downloadData(baseUrl + "/interfaz/unica/recibo/" + interfaz.referencia + "?v=" + Helper.VERSION);
                        interfaz.gotRecibo();
                        break;
                    default:
                        MessageBox.Show("Hubo un error inesperado en la ejecución.");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e);
                interfaz.onErrorRecibo(e.Message);
            }
        }

        /* Exchange authorization token */
        public static async void goToCMS()
        {
            try
            {
                Dictionary<string, string> request = await ApiHelper.getCMSToken();
                String queryString = "?";
                foreach (string key in request.Keys)
                {
                    if (queryString != "")
                    {
                        queryString += "&";
                    }
                    queryString += key + "=" + HttpUtility.UrlEncode(request[key]);
                }
                Process.Start(@cmsUrl + @"/#/login" + @queryString);
            }
            catch (Exception err)
            {
                MessageBox.Show(ApiHelper.translateError(err.Message));
            }
        }

        public static async void goToChangePass()
        {
            try
            {
                Process.Start(@cmsUrl + @"#/user/changePassword?err=pass-expired");
            }
            catch (Exception err)
            {
                MessageBox.Show(ApiHelper.translateError(err.Message));
            }
        }

        private static async Task<Dictionary<String, String>> getCMSToken()
        {
            Dictionary<string, string> request = new Dictionary<string, string>();
            request.Add("client_id", cmsClientId);
            request.Add("client_secret", cmsClientSecret);
            var jsonString = JsonConvert.SerializeObject(request);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            string responseString = await postData(baseUrl + "/oauth/token/change", content, true);
            var json = JsonConvert.DeserializeObject<dynamic>(responseString);
            request.Clear();
            request.Add("access_token", (string) json.access_token);
            request.Add("refresh_token", (string) json.refresh_token);
            request.Add("expires_in", (string) json.expires_in);
            request.Add("token_type", (string) json.token_type);
            return request;
        }

        /* Authorization token */
        public static async Task<String> login(String user, String password)
        {
            string error = await requestToken(new Dictionary<string, string>
                {
                    { "username", user },
                    { "password", password },
                    { "grant_type", "password" }
                }
            );

            return error;
        }

        public static async Task<String> logout()
        {
            string responseString = await getData(baseUrl + "/user/logout", true);
            dynamic json = JsonConvert.DeserializeObject<dynamic>(responseString);
            if (json.error is JArray)
            {
                json = json.error.First;
            }
            return json.error;
        }

        public static async Task<String> refresh()
        {
            if (token == null)
            {
                return "not-logged";
            } else
            {
                return await requestToken(new Dictionary<string, string>
                    {
                        { "refresh_token", token.refreshToken },
                        { "grant_type", "refresh_token" }
                    }
                );
            }
        }

        public static async Task<AuthenticationHeaderValue> getAuthenticationHeader()
        {
            if (token == null)
            {
                throw new Exception("not-logged");
            }
            if (token.hasExpired())
            {
                string error = await refresh();
                if (error != null)
                {
                    throw new Exception(error);
                }
            }
            return token.authorizationHeader;
        }

        private static async Task<string> requestToken(Dictionary<string, string> request)
        {
            string error = null;
            request.Add("client_id", clientId);
            request.Add("client_secret", clientSecret);
            var jsonString = JsonConvert.SerializeObject(request);
            var content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            try
            {
                var responseString = await postData(baseUrl + "/oauth/token", content);
                try
                {
                    token = new ApiToken(responseString);
                }
                catch (Exception e)
                {
                    token = null;
                    error = e.Message;
                }
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            return error;
        }

        private static void handleCompletarResult(Interfaz interfazDone, dynamic json)
        {
            File.WriteAllText(interfazDone.path, (string)json.data);
            sendSignedFile(interfazDone);
        }

        private static void handleGuardarResult(Interfaz interfazDone, dynamic json)
        {
            interfacesAwaitingResponse.Remove((string)json.filename);
            interfazDone.onWarning(json.warning);
            interfazDone.sendedFile((string)json.data);

            // Rename file.
            string newFileName = interfazDone.relativePath + interfazDone.generateFileName();
            Console.WriteLine("{0} + {1} = {2}", interfazDone.relativePath, interfazDone.generateFileName(), newFileName);
            File.Move(interfazDone.path, newFileName);
            interfazDone.changePath(newFileName);
        }

        private static string generateErrorString(JToken error)
        {
            string errorString = "";

            try
            {
                errorString = (string) error["error"];
                try
                {
                    if (error["data"] != null)
                    {
                        errorString += "|" + (string)error["data"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    errorString = (string)error["error"];
                }
            } catch (Exception ex)
            {
                errorString = (string) error;
            }
            
            return errorString;
        }

        public static void setupWebSocket(Interfaces uiThread)
        {
            socketHelper = new SocketHelper(baseSocketUrl, token);

            socketHelper.onMessage += (sender, e) =>
            {
                uiThread.Invoke(new MethodInvoker(delegate {
                    var json = e.Data;
                    try {
                        var interfazDone = interfacesAwaitingResponse[(string)json.filename];
                        interfazDone.resetErrores();
                        if (interfazDone == null)
                        {
                            return;
                        }
                        if (json["error"] != null)
                        {
                            if (json.error is JArray)
                            {
                                var errorArray = json.error;
                                foreach (var oneError in errorArray)
                                {
                                    string error = generateErrorString(oneError);
                                    Console.WriteLine("Each error in array: {0}", error);
                                    interfazDone.onError(error);
                                }
                            } else
                            {
                                string error = generateErrorString(json);
                                Console.WriteLine("Error: {0}", error);
                                interfazDone.onError(error);
                            }

                            interfacesAwaitingResponse.Remove((string)json.filename);
                            return;
                        }
                        switch (e.Event)
                        {
                            case "completarResult":
                                handleCompletarResult(interfazDone, json);
                                break;
                            case "guardarResult":
                                handleGuardarResult(interfazDone, json);
                                break;
                            default:
                                Console.WriteLine("Got an unexpected event: {0}", e.Event);
                                break;
                        }
                    } catch (Exception excp)
                    {
                        Console.WriteLine("Error on onMessage event listener: {0}", excp.Message);
                    }
                }));
            };

            socketHelper.onClose += (sender, e) =>
            {
                uiThread.Invoke(new MethodInvoker(delegate
                {
                    cancelRunningTasks();
                }));
            };
        }

        public static void cancelRunningTasks()
        {
            foreach (var interfazDone in interfacesAwaitingResponse.Values)
            {
                interfazDone.onError("internal");
            }
            interfacesAwaitingResponse.Clear();
        }
    }
}
